var shell = require('shelljs');
var args = require('yargs').argv;
var semverregex = require('semver-regex');

const defaultVer = 'minor';
var versionArg = args._[0] || defaultVer;
console.log(versionArg);
if(args._.length > 1){
  console.log('Wrong no of arguments');
  shell.exit(1);
}
if(versionArg !== 'minor' && versionArg !== 'patch' && versionArg !== 'major' ){
  var semVerVersionExists = semverregex().test(versionArg);
  if(!semVerVersionExists){
    console.log('Usage: node release [semver_version_number|patch|minor|major]');
    shell.exit(1);
  }
}

//upgrade version
var versionUpgradeOutput = shell.exec('npm version ' + versionArg );
if(versionUpgradeOutput.code !== 0){
  shell.exit(1);
}
//publish the packaeg
var publishOutput = shell.exec('npm publish');
if(publishOutput.code !== 0){
  shell.exit(1);
}
