const fs = require('fs-extra');
const concat = require('concat');
const angularConfig = require('./angular.json');
const packageJson = require('./package.json');

const outputFileName = packageJson.name + packageJson.version + '.js';
const outputFolderPath = angularConfig.projects[angularConfig.defaultProject].architect.build.options.outputPath;
const outputFilePath = outputFolderPath +'/' + outputFileName;



(async function build() {
  const files =[
    './dist/runtime.js',
    './dist/polyfills.js',
    './dist/main.js',
    './dist/styles.js'
  ];

  await fs.ensureDir('elements');

  await concat(files, outputFilePath)
})();

