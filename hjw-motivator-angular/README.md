# HjwMotivatorAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

To run app in local environment with local backend services set up -
Run `ng serve -c local` 
you might have to change environment.local.ts to make API_HOSTS object point to you local services.

To run app in local environment with backendless development in mock mode -
Run `ng serve -c local-mock` 

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build ` to build the project for production. The build artifacts will be stored in the `dist/` directory.
this will carry out two steps :
1) build project in production mode
2) post process built files and bundle runtime.js styles.js and main.js into one single file {appname}+{appversion}.js
   where appname and appversion are picked up from package.json. 
## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## HOW TO PUBLISH to NPM REPO
To publish the app run following command from hjw-motivator-angular directory:
node release [semver_version_number|patch|minor|major]
If you dont pass any argument it will increment minor version by defualt.
Above command publishes it to nordea NPM registry. Visit http://npm.nd.dev01.qaoneadr.local and make sure 
if its published to make sure.
Please use following command before you run the command for the first time.
     npm adduser --registry http://npm.nd.dev01.qaoneadr.local
     
it asks for username and password and your email. Remember the password. once your are logged in you can publish the 
artifact.

## Further help
For environments other than local API_HOST property must be set to backend host. for every environment there
is individual environment file created. e.g for dev environemnt configured in angular.json there environment.dev.ts.
We can create custom environments as per the need. Just make sure to add environment.{environment_name}.ts and 
corresponding configuration in angular.json

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
