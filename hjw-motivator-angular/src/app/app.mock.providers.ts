import {CreditAssessmentServiceMockInterceptor} from './services/credit-assessment.service.mock';
import {FinancialOverviewServiceMockInterceptor} from './services/financial-overview.service.mock';
import {ModelBudgetServiceMockInterceptor} from './services/model-budget.service.mock';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

export const ServiceMocks = [
  {
    provide: HTTP_INTERCEPTORS, useClass: CreditAssessmentServiceMockInterceptor, multi: true
  },
  {
    provide: HTTP_INTERCEPTORS, useClass: FinancialOverviewServiceMockInterceptor, multi: true
  },
  {
    provide: HTTP_INTERCEPTORS, useClass: ModelBudgetServiceMockInterceptor, multi: true
  }
];
