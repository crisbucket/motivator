import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './page-components/dashboard/dashboard.component';
import {NgModule} from '@angular/core';
import {AdultsAndIncomeComponent} from './page-components/adults-and-income/adults-and-income.component';
import {WelcomeAssessmentComponent} from './page-components/welcome-assessment/welcome-assessment.component';
import {ChildrenComponent} from './page-components/children/children.component';
import {CarsComponent} from './page-components/cars/cars.component';
import {PropertyComponent} from './page-components/property/property.component';

const routes: Routes = [
  {path: 'dashboard', component: DashboardComponent},
  {path: '', component: WelcomeAssessmentComponent},
  {path: 'adults', component: AdultsAndIncomeComponent},
  {path: 'children', component: ChildrenComponent},
  {path: 'cars', component: CarsComponent},
  {path: 'property', component: PropertyComponent},
  {path: '**', component: WelcomeAssessmentComponent},
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
