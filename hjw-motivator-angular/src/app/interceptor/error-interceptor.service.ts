import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {Observable, of} from 'rxjs';
import {Router} from "@angular/router";
import {catchError} from "rxjs/internal/operators";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private router: Router) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(error => {
        if (error instanceof HttpErrorResponse) {
          console.log(error);
          this.handleError(error.status);
        }
        return of(error);
      })
    );
  }

  handleError(code: number) {
    switch (code) {
      case 404: {
        this.clearStorages();
        this.navigateToMainPage();
        break;
      }
      default: {
        break;
      }
    }
  }

  private clearStorages() {
    sessionStorage.clear();
  }

  private navigateToMainPage() {
    this.router.navigate(['']);
  }
}
