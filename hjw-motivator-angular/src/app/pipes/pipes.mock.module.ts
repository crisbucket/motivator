import {NgModule} from '@angular/core';

import {MockReplaceLabelPipe} from "./replace-label.pipe.mock";
import {MockNumberAmountPipe} from "./number-amount.pipe.mock";

@NgModule({
  declarations: [
    MockReplaceLabelPipe,
    MockNumberAmountPipe
  ],
  exports: [
    MockReplaceLabelPipe,
    MockNumberAmountPipe
  ]
})
export class PipesMockModule {}
