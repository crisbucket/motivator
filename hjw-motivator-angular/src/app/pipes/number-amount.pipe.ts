import { Pipe, PipeTransform } from '@angular/core';
import { formatNumber } from '@angular/common';

@Pipe({
  name: 'numberAmount'
})
export class NumberAmountPipe implements PipeTransform {
  private readonly LOCALE = 'se';

  transform(value: number): string {
    if (value !== undefined && value !== null) {
      return formatNumber(value, this.LOCALE);
    } else {
      return '';
    }
  }
}

