import { Pipe, PipeTransform } from '@angular/core';
import { ConfigService } from '../services/config.service';
import { NordicMotivatorApp } from '../nordic-motivator-app';

@Pipe({name: 'replaceLabel'})
export class ReplaceLabelPipe implements PipeTransform {

  private labels = [];

  constructor(private config: ConfigService, private hjnmTridionApp: NordicMotivatorApp) {
    this.labels = hjnmTridionApp.getLabels();
  }

  transform(value: string): string {
    const labelValue = this.labels[value];

    if (labelValue) {
      return labelValue;
    }
    return '';
  }
}
