import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'format'
})
export class FormatPipe implements PipeTransform {

  private readonly SEARCH_PATTERN = /{(\d+)}/g;

  transform(value: string, ...args: any[]): any {
    if (!args) {
      return value;
    }
    return value.replace(this.SEARCH_PATTERN, (substring) => {
      const index = +substring.replace(/{/, '').replace(/}/, ''); // for some reason cant get it work with one replace
      return args[index] || substring;
    });
  }

}

