import {Pipe, PipeTransform} from "@angular/core";

@Pipe({name: 'replaceLabel'})
export class MockReplaceLabelPipe implements PipeTransform {
  transform(value: string): string {
    return value;
  }
}
