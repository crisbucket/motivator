import { FormatPipe } from './format.pipe';

describe('FormatPipe', () => {
  it('create an instance', () => {
    const pipe = new FormatPipe();
    expect(pipe).toBeTruthy();
  });

  it('replace parameters', () => {
    const pipe = new FormatPipe();
    const given = 'pierwszy: {0}, drugi: {1}, znowu pierwszy: {0}, trzeci ale go nie ma: {3}';
    const expected = 'pierwszy: pierwszy argument, drugi: drugi argument, znowu pierwszy: trzeci argument, trzeci ale go nie ma: {3}';
    expect(pipe.transform(given, 'pierwszy argument', 'drugi argument'));
  });
});
