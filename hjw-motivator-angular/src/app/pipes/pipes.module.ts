import {NgModule} from '@angular/core';
import {ReplaceLabelPipe} from "../pipes/replace-label.pipe";
import {FormatPipe} from "../pipes/format.pipe";
import {NumberAmountPipe} from "../pipes/number-amount.pipe";

@NgModule({
  declarations: [
    ReplaceLabelPipe,
    FormatPipe,
    NumberAmountPipe,
  ],
  exports: [
    ReplaceLabelPipe,
    FormatPipe,
    NumberAmountPipe,
  ]
})
export class PipesModule { }
