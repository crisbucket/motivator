import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'numberAmount'
})
export class MockNumberAmountPipe implements PipeTransform {

  transform(value: number): string {
    return value.toString();
  }
}

