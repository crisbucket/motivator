export class Expense {
  public static GROUP_TYPE_TRANSLATIONS = {
    CLOTHES: 'clothes',
    FREE_TIME_EXPENSES: 'free time expenses',
    FOOD: 'food',
    HEALTH_CARE: 'health care',
    OTHER: 'other',
    INSURANCE: 'insurance',
    HOUSING_EXPENSES: 'housing expences',
    PUBLIC_TRANSPORTATION_COSTS: 'public transportation costs',
  };
  amount: number;
  category: string;
}
