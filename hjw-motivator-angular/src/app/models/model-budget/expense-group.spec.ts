import {ExpenseGroup} from "./expense-group";

describe('ExpenseGroup', () => {
  it('recalculateAmount() should return 0 for no expenseLabels', () => {
    let expenseGroup = new ExpenseGroup();
    expenseGroup.recalculateAmount();
    expect(expenseGroup.totalAmount).toEqual(0);
  });

  it('recalculateAmount() should sum up expenseLabels', () => {
    let expenseGroup = new ExpenseGroup();
    expenseGroup.expenses = [{amount: 1, category: ""}, {amount: 2, category: ""}];
    expenseGroup.recalculateAmount();
    expect(expenseGroup.totalAmount).toEqual(3);
  });
});
