import {ExpenseGroup} from "./expense-group";

export class ModelBudget {
  expenseGroups: ExpenseGroup[] = [];

  constructor(expenseGroups: ExpenseGroup[]) {
    this.expenseGroups = expenseGroups;
  }
}
