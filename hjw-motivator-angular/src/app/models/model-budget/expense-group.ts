import {Expense} from './expense';

export class ExpenseGroup {
  subgroups: ExpenseGroup[] = [];
  expenses: Expense[] = [];
  groupType: string;
  totalAmount: number;

  public recalculateAmount(): void {
    this.totalAmount = this.getTotalAmount();
  }

  public getTotalAmount(): number {
    return this.expenses.map(e => e.amount).reduce((a, b) => a + b, 0);
  }
}
