export class Adult {
  constructor(public incomeBeforeTaxation: number = 0, public incomeAfterTaxation: number = 0) {}
}
