import {Adult} from "./adult";
import {Child} from "./child";

export class FamilySituation {
  adults: Adult[] = [];
  children: Child[] = [];

  constructor(adults: Adult[], children: Child[]) {
    this.adults = adults;
    this.children = children;
  }
}
