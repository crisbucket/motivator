import {Adult} from './adult';
import {Child} from './child';
import {Country} from '../../country';

export class HouseholdSituation {
  adults: Adult[] = [];
  children: Child[] = [];
  numberOfCars = 0;
  otherTransportationCost = 0;
  country = Country.DK;
}
