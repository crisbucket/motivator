import {DebtType} from "./debt-type";
import {DebtMonthlyPayment} from "../dashboard/debt-monthly-payment";

export class Debt {
  amount: number;
  type: DebtType;
  monthlyPaymentByMarketRate: number = 0;
  monthlyPaymentByStressedTest: number = 0;

  constructor(type: DebtType, amount: number) {
    this.type = type;
    this.amount = amount;
  }

  updateWith(debtMonthlyPayment: DebtMonthlyPayment) {
    this.monthlyPaymentByMarketRate = debtMonthlyPayment.monthlyPaymentByMarketRate;
    this.monthlyPaymentByStressedTest = debtMonthlyPayment.monthlyPaymentByStressedTest;
  }
}
