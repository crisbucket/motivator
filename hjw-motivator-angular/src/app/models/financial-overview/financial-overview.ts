import {Debt} from './debt';
import {HouseholdSituation} from './householdsituation/household-situation';
import {ExpenseGroup} from '../model-budget/expense-group';
import {DebtMonthlyPayment} from "../dashboard/debt-monthly-payment";
import {Adult} from "./householdsituation/adult";

export class FinancialOverview {
  id: string;
  totalDebt: number;
  debts: Debt[] = [];
  childrenSupportIncome: number;
  expenseGroups: ExpenseGroup[] = [];
  downPayment: number;
  totalAffordability: number;
  loanAmount: number;
  monthlyLoanCost: number;
  totalExpenses: number;
  householdSituation: HouseholdSituation = new HouseholdSituation();

  public updateMonthlyPaymentForDebt(debtType: string, debtMonthlyPayment: DebtMonthlyPayment) {
    let index = this.debts.findIndex(d => d.type == debtType);
    this.debts[index].updateWith(debtMonthlyPayment);
  }

  public saveOrUpdateExpenseGroup(expenseGroup: ExpenseGroup) {
    let index: number = this.expenseGroups.findIndex(group => group.groupType == expenseGroup.groupType);
    var existingGroup = this.findExpenseGroupByType(expenseGroup.groupType);
    if (existingGroup == null) {
      this.expenseGroups.push(expenseGroup);
    } else {
      this.expenseGroups[index] = expenseGroup;
    }
    this.recalculateTotalExpenses();
    console.log(this.expenseGroups);
  }

  public updateAdults(adults: Adult[]) {
    this.householdSituation.adults = adults;
  }

  public saveOrUpdateDebt(debt: Debt) {
    var existingDebt = this.findDebtByType(debt.type);
    if (existingDebt == null) {
      this.debts.push(debt);
    } else {
      existingDebt = debt;
    }
  }

  recalculateTotalExpenses() {
    this.totalExpenses = this.expenseGroups.map(function (expenseGroup) {
      return Object.assign(new ExpenseGroup(), expenseGroup as ExpenseGroup).getTotalAmount();
    }).reduce(function (i1, i2) {
      return i1 + i2;
    });
  }

  getTotalDebt(): number {
    return this.debts
      .map((debt) => debt.amount)
      .reduce((a, b) => a + b, 0);
  }

  setIncome(income: number) {
    this.childrenSupportIncome = income;
  }

  addDebt(debt: Debt) {
    this.debts.push(debt);
  }

  setMonthlyLoanCost(monthlyLoanCost: number) {
    this.monthlyLoanCost = monthlyLoanCost;
  }

  setLoanAmount(loanAmount: number) {
    this.loanAmount = loanAmount;
  }

  setTotalAffordability(totalAffordability: number) {
    this.totalAffordability = totalAffordability;
  }

  setDownPayment(downPayment: number) {
    this.downPayment = downPayment;
  }

  public findExpenseByGroupTypeAndCategory(groupType: string, category: string) {
    let expenseGroup = this.findExpenseGroupByType(groupType);
    return expenseGroup != undefined ? expenseGroup.expenses.find(expense => expense.category == category) : undefined;
  }

  private findExpenseGroupByType(type: string) {
    return this.expenseGroups.find(group => group.groupType == type);
  }

  public findDebtByType(type: string) {
    return this.debts.find(debt => debt.type == type);
  }

  getTotalIncomeAfterTax(): number {
    return this.householdSituation.adults.map(adult => {
      return adult.incomeAfterTaxation;
    }).reduce((i1, i2) => {
      return i1 + i2;
    });
  }

  getTotalIncomeAfterTaxWithChildSupport(): number {
    return this.getTotalIncomeAfterTax() + this.childrenSupportIncome;
  }

  toAPI() {
    let financialOverview = this;
    delete financialOverview.totalDebt;
    return financialOverview;
  }
}
