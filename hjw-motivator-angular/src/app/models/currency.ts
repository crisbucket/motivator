export enum Currency {
  EUR = "EUR",
  DKK = "DKK",
  NOK = "NOK",
  SEK = "SEK",
}

