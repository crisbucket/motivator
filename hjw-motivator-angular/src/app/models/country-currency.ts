import {Currency} from "./currency";
import {Country} from "./country";

export class CountryCurrency {
  static getByCountry(country: Country): Currency {
    switch (country) {
      case Country.DK:
        return Currency.DKK;
      case Country.NO:
        return Currency.NOK;
      case Country.FI:
        return Currency.EUR;
      case Country.SE:
        return Currency.SEK;
    }
  }
}

