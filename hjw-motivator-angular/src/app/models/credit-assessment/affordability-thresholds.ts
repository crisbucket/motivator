export class AffordabilityThresholds {
  affordable: number;
  stretched: number;
  max: number;
}
