
export class CreditAssessment {
  totalAffordability: number = 0;
  totalLoan: number = 0;
  totalAffordabilityOnDti: number = 0;
  minDownPaymentForDtiAffordability: number = 0;
  downPaymentLimit: number = 0;
}
