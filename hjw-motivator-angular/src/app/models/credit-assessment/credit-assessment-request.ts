import {Country} from "../country";

export class CreditAssessmentRequest {
  constructor(private income: number, private debt: number, private downpayment: number, private country: Country) {}
}
