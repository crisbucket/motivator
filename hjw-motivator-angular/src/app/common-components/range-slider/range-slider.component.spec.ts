import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RangeSliderComponent} from './range-slider.component';
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material.module";

describe('RangeSliderComponent', () => {
  let component: RangeSliderComponent;
  let fixture: ComponentFixture<RangeSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RangeSliderComponent ],
      imports: [ MaterialModule, FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RangeSliderComponent);
    component = fixture.componentInstance;
    component.value = 3;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
