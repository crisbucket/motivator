import {Component, ElementRef, Input, OnInit, Renderer2} from '@angular/core';

export interface RangeDescription {
  description: string;
  value: number;
  color: string;
}

@Component({
  selector: 'hj-range-slider',
  templateUrl: './range-slider.component.html',
  styleUrls: ['./range-slider.component.scss']
})
export class RangeSliderComponent implements OnInit {
  private _value = 0;
  private _currentRange: RangeDescription;

  @Input()
  ranges: RangeDescription[] = [{
    description: 'Affordable',
    value: 50,
    color: 'limegreen'
  }, {
    description: 'Less likely',
    value: 100,
    color: 'blue'
  },
    {
      description: 'Unrealistic',
      value: 150,
      color: 'red'
    }];

  constructor(private _renderer: Renderer2, private _el: ElementRef) {
  }

  get value(): number {
    return this._value;
  }

  set value(value: number) {
    this._value = value;
    this._currentRange = this.getCurrentRange();
    this._renderer.setAttribute(this._el.nativeElement, 'style', this.style());
  }

  get color() {
    return this._currentRange.color;
  }

  get description() {
    return this._currentRange.description;
  }

  ngOnInit() {
    this._renderer.setAttribute(this._el.nativeElement, 'style', this.style());
  }

  private getCurrentRange(): RangeDescription {
    for (const descr of this.ranges) {
      if (this._value <= descr.value) {
        return descr;
      }
    }
    return null;
  }

  private style() {
    return `--fill-c: ${this._currentRange.color}; --val: ${this._value};`;
  }

}
