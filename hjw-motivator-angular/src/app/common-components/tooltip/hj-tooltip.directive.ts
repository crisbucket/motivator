import { Directive, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { HjTooltipContainerProviderDirective } from './hj-tooltip-container-provider.directive';
import { HjTooltipPosition, HjTooltipType, TooltipOptions } from './tooltip-container/hj-tooltip-container.component';

@Directive({
  exportAs: 'hjTooltip',
  selector: '[hjTooltip]'
})
export class HjTooltipDirective implements OnChanges {
  @Input()
  hjTooltip = '';
  @Input()
  hjTooltipType = HjTooltipType.INFORMATION;
  @Input()
  hjTooltipPosition = HjTooltipPosition.BELOW;
  @Output()
  hjTooltipVisibleChange = new EventEmitter<boolean>();
  @Input()
  hjTooltipSticky = false;
  private _tooltipOptions: TooltipOptions;

  constructor(private _hjTooltipContainerProviderDirective: HjTooltipContainerProviderDirective, private _el: ElementRef) {
    this._tooltipOptions = {
      id: Math.random(),
      ref: this._el.nativeElement,
      type: this.hjTooltipType,
      text: this.hjTooltip,
      position: this.hjTooltipPosition,
      sticky: this.hjTooltipSticky
    };
    this.updateTooltipContainer();
    this._hjTooltipContainerProviderDirective.hjTooltipContainer.visibilityChange$.subscribe((change: { id: number, visible: boolean }) => {
      if (change.id === this._tooltipOptions.id) {
        this._hjTooltipVisible = change.visible;
        this.hjTooltipVisibleChange.emit(change.visible);
      }
    });

    if (!_hjTooltipContainerProviderDirective || !_hjTooltipContainerProviderDirective.hjTooltipContainer) {
      console.error('no container for tooltip available!');
    }
  }

  private _hjTooltipVisible = false;

  @Input()
  set hjTooltipVisible(value: boolean) {
    if (this._hjTooltipVisible !== value) {
      this._hjTooltipVisible = value;
      this._hjTooltipContainerProviderDirective.hjTooltipContainer.toggleTooltip(this._tooltipOptions.id, this._hjTooltipVisible);
    }

  }

  ngOnChanges(changes: SimpleChanges): void {
    const {hjTooltip, hjTooltipType, hjTooltipPosition, hjTooltipSticky, hjTooltipVisible} = changes;
    if (hjTooltip || hjTooltipType || hjTooltipPosition || hjTooltipSticky) {
      this.updateTooltip();
    }
  }

  updateTooltip() {
    this._tooltipOptions = {
      ...this._tooltipOptions,
      ref: this._el.nativeElement,
      type: this.hjTooltipType,
      text: this.hjTooltip,
      position: this.hjTooltipPosition,
      sticky: this.hjTooltipSticky
    };
    this.updateTooltipContainer();
  }

  toggleTooltip() {
    this._hjTooltipContainerProviderDirective.hjTooltipContainer.toggleTooltip(this._tooltipOptions.id);
  }

  private updateTooltipContainer() {
    this._hjTooltipContainerProviderDirective.hjTooltipContainer.updateTooltip(this._tooltipOptions);
  }

}
