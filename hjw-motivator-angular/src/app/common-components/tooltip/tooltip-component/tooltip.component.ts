import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output
} from '@angular/core';
import {HjTooltipPosition, HjTooltipType, TooltipOptions} from '../tooltip-container/hj-tooltip-container.component';

@Component({
  selector: 'hj-tooltip-component',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.scss']
})
export class HjTooltipComponent implements OnInit, AfterViewInit {
  @Input()
  hjTooltipOptions: TooltipOptions;
  @Input()
  hjTooltipHostEl: HTMLElement;
  @Input()
  hjTooltipVisible: boolean;
  @Output()
  hjTooltipHide = new EventEmitter();

  _elementInitialised = false;

  constructor(private _changeDetector: ChangeDetectorRef, private _el: ElementRef) {
  }

  private _position: { top: number, left: number };

  get position(): { top: number; left: number } {
    return this._position;
  }

  @HostBinding('style.top')
  get translation() {
    return `${this._position.top}px`;
  }

  @HostBinding('class.hj-tooltip-information')
  get InfoClass() {
    return this.hjTooltipOptions.type === HjTooltipType.INFORMATION;
  }

  @HostBinding('class.hj-tooltip-warning')
  get WarningClass() {
    return this.hjTooltipOptions.type === HjTooltipType.WARNING;
  }

  @HostBinding('class.hj-tooltip-alert')
  get AlertClass() {
    return this.hjTooltipOptions.type === HjTooltipType.ALERT;
  }

  @HostBinding('class.hj-tooltip-above')
  get AboveClass() {
    return this.hjTooltipOptions.position === HjTooltipPosition.ABOVE;
  }

  @HostBinding('class.hj-tooltip-below')
  get BelowClass() {
    return this.hjTooltipOptions.position === HjTooltipPosition.BELOW;
  }

  @HostBinding('class.hj-tooltip-visible')
  get VisibleClass() {
    return this.hjTooltipVisible === true;
  }

  @HostBinding('class.hj-tooltip-hidden')
  get HiddenClass() {
    return this.hjTooltipVisible === false;
  }

  private static offset(nativeEl: any): { width: number, height: number, top: number, left: number } {
    const boundingClientRect = nativeEl.getBoundingClientRect();
    return {
      width: boundingClientRect.width || nativeEl.offsetWidth,
      height: boundingClientRect.height || nativeEl.offsetHeight,
      top: boundingClientRect.top + (window.pageYOffset || window.document.documentElement.scrollTop),
      left: boundingClientRect.left + (window.pageXOffset || window.document.documentElement.scrollLeft)
    };
  }

  private static positionElements(hostEl: HTMLElement, targetEl: HTMLElement, position: HjTooltipPosition): { top: number, left: number } {

    const hostElPos = HjTooltipComponent.offset(hostEl);
    const targetElPos = HjTooltipComponent.offset(targetEl);
    const targetElWidth = targetEl.offsetWidth;
    const targetElHeight = targetEl.offsetHeight;

    let elementPosition: { top: number, left: number };

    switch (position) {
      case HjTooltipPosition.ABOVE:
        elementPosition = {
          top: targetElPos.top - hostElPos.top,
          left: targetElPos.left - hostElPos.left + targetElWidth / 2
        };
        break;
      case HjTooltipPosition.BELOW:
        elementPosition = {
          top: targetElPos.top - hostElPos.top + targetElHeight,
          left: targetElPos.left - hostElPos.left + targetElWidth / 2
        };
        break;
    }

    return elementPosition;
  }

  ngOnInit() {
    this.recalculatePosition();
  }

  @HostListener('window:resize')
  onWindowResize() {
    this.recalculatePosition();
  }

  notchStyle() {
    return {left: `${this.position.left}px`};
  }

  hideTooltip() {
    this.hjTooltipHide.emit({id: this.hjTooltipOptions.id});
  }

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedInsideOrInRefElement = this._el.nativeElement.contains(targetElement) || targetElement === this.hjTooltipOptions.ref;
    if (!clickedInsideOrInRefElement && this.hjTooltipVisible === true && this.hjTooltipOptions.sticky === false) {
      this.hideTooltip();
    }
  }

  ngAfterViewInit(): void {
    this._elementInitialised = true;
  }

  private recalculatePosition() {
    this._position = HjTooltipComponent
      .positionElements(this.hjTooltipHostEl, this.hjTooltipOptions.ref, this.hjTooltipOptions.position);
    this._changeDetector.markForCheck();
  }
}
