import { Directive, Host, Input, OnChanges, Optional, SimpleChanges } from '@angular/core';
import { HjTooltipContainerComponent } from './tooltip-container/hj-tooltip-container.component';

@Directive({
  exportAs: 'hjTooltipContainerProvider',
  selector: 'hj-tooltip-container, [hjTooltipContainerProvider]'
})
export class HjTooltipContainerProviderDirective implements OnChanges {
  @Input()
  hjTooltipContainerProvider: HjTooltipContainerComponent;

  constructor(@Host() @Optional() private hjTooltipContainerComponent: HjTooltipContainerComponent) {
  }

  private _hjTooltipContainer = this.hjTooltipContainerComponent;

  get hjTooltipContainer(): HjTooltipContainerComponent {
    return this._hjTooltipContainer;
  }

  ngOnChanges(changes: SimpleChanges) {
    const {hjTooltipContainerProvider} = changes;
    if (hjTooltipContainerProvider) {
      this._hjTooltipContainer = this.hjTooltipContainerProvider || this.hjTooltipContainerComponent;
    }
  }

}
