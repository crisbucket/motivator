import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {HjTooltipDirective} from "./hj-tooltip.directive";
import {HjTooltipContainerComponent} from "./tooltip-container/hj-tooltip-container.component";
import {HjTooltipContainerProviderDirective} from "./hj-tooltip-container-provider.directive";
import {HjTooltipComponent} from "./tooltip-component/tooltip.component";

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    HjTooltipDirective,
    HjTooltipContainerComponent,
    HjTooltipContainerProviderDirective,
    HjTooltipComponent,
  ],
  exports: [
    HjTooltipDirective,
    HjTooltipContainerComponent,
    HjTooltipContainerProviderDirective,
    HjTooltipComponent
  ]
})
export class TooltipModule {
}
