import { Component, ElementRef, Input, OnInit } from '@angular/core';
import { Subject } from "rxjs/Subject";

export enum HjTooltipType {
  INFORMATION, WARNING, ALERT
}

export enum HjTooltipPosition {
  ABOVE, BELOW
}

export interface TooltipOptions {
  id: number;
  ref: HTMLElement;
  text: string;
  position: HjTooltipPosition;
  type: HjTooltipType;
  sticky: boolean;
}

@Component({
  selector: 'hj-tooltip-container',
  templateUrl: './hj-tooltip-container.component.html',
  styleUrls: ['./hj-tooltip-container.component.scss']
})
export class HjTooltipContainerComponent implements OnInit {

  @Input()
  name;

  nativeEl: HTMLElement = this._el.nativeElement;
  private _isVisible: Map<number, boolean> = new Map<number, boolean>();

  private _visibilityChange: Subject<{ id: number, visible: boolean }> = new Subject<{ id: number, visible: boolean }>();

  constructor(private _el: ElementRef) {
  }

  get visibilityChange$() {
    return this._visibilityChange.asObservable();
  }

  private _tooltips: TooltipOptions[] = [];

  get tooltips() {
    return this._tooltips;
  }

  ngOnInit() {
  }

  public updateTooltip(tooltipOptions: TooltipOptions) {
    const index = this.findTooltipIndex(tooltipOptions.id);
    if (index >= 0) {
      this.tooltips[index] = tooltipOptions;
    } else {
      this.tooltips.push(tooltipOptions);
    }
  }

  public toggleTooltip(id: number, forced?: boolean) {
    this._tooltips.forEach(tooltip => {
      let visible = this.isTooltipVisible(tooltip.id);
      if (tooltip.id === id) {
        if (forced) {
          visible = forced;
        } else {
          visible = !visible;
        }
      } else {
        visible = visible && tooltip.sticky;
      }
      this.updateTooltipVisibility(tooltip.id, visible);
    });
  }

  isTooltipVisible(id: number): boolean {
    return this._isVisible.get(id) || false;
  }

  updateTooltipVisibility(id: number, visible: boolean) {
    const initialVisibility = this.isTooltipVisible(id);
    this._isVisible.set(id, visible);
    if (initialVisibility !== visible) {
      this._visibilityChange.next({id, visible});
    }
  }

  private findTooltipIndex(id: number) {
    return this.tooltips.findIndex(tooltip => tooltip.id === id);
  }

}
