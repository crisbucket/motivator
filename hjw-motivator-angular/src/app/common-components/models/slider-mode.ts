export enum SliderMode {
  SPECIFIC_RANGE = 0,
  LINEAR_RANGE = 1
}
