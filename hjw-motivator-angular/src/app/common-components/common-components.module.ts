import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CounterComponent} from './counter/counter.component';
import {HjValueSliderComponent} from './hj-value-slider/hj-value-slider.component';
import {RangeSliderComponent} from './range-slider/range-slider.component';
import {AffordabilitySliderComponent} from './affordability-slider/affordability-slider.component';
import {MatDialogModule, MatSliderModule} from '@angular/material';
import {ProgressDotsComponent} from '../page-components/progress/progress-dots.component';
import {HjSwitchComponent} from './hj-switch/hj-switch.component';
import {PipesModule} from '../pipes/pipes.module';
import {SliderDialogComponent} from './hj-value-slider/slider-dialog/slider-dialog.component';
import {TooltipModule} from "./tooltip/tooltip.module";

@NgModule({
  entryComponents: [
    SliderDialogComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MatSliderModule,
    MatDialogModule,
    PipesModule,
    TooltipModule
  ],
  declarations: [
    HjValueSliderComponent,
    CounterComponent,
    ProgressDotsComponent,
    RangeSliderComponent,
    AffordabilitySliderComponent,
    HjSwitchComponent,
    SliderDialogComponent,
    HjSwitchComponent,
  ],
  exports: [
    HjValueSliderComponent,
    CounterComponent,
    ProgressDotsComponent,
    RangeSliderComponent,
    AffordabilitySliderComponent,
    HjSwitchComponent,
    SliderDialogComponent,
    TooltipModule
  ]
})
export class CommonComponentsModule {
}
