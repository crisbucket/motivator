import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HjValueSliderComponent} from "./hj-value-slider.component";
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material.module";
import {PipesMockModule} from "../../pipes/pipes.mock.module";
import {MatDialog} from '@angular/material';
import {TooltipModule} from "../tooltip/tooltip.module";

describe('HjnSliderComponent', () => {
  let component: HjValueSliderComponent;
  let fixture: ComponentFixture<HjValueSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HjValueSliderComponent ],
      providers: [{ provide: MatDialog, useClass: MatDialog },],
      imports: [ MaterialModule, FormsModule, PipesMockModule, TooltipModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HjValueSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
