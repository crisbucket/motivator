import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import {Component, Inject} from "@angular/core";

export interface DialogData {
  description: string;
  subDescription: string;
  selectedValue: number;
  min: number;
  max: number;
}

@Component({
  selector: 'hj-slider-dialog',
  templateUrl: './slider-dialog.component.html',
})
export class SliderDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<SliderDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onCloseClick(): void {
    this.dialogRef.close();
  }

  digitsOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  }
}
