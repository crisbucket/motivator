import {Component, EventEmitter, forwardRef, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {SliderMode} from '../models/slider-mode';
import {SliderDialogComponent} from './slider-dialog/slider-dialog.component';
import {HjTooltipDirective} from '../tooltip/hj-tooltip.directive';
import {HjTooltipPosition} from '../tooltip/tooltip-container/hj-tooltip-container.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'hj-value-slider',
  templateUrl: './hj-value-slider.component.html',
  styleUrls: ['./hj-value-slider.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HjValueSliderComponent),
      multi: true
    }
  ]
})
export class HjValueSliderComponent implements OnChanges, ControlValueAccessor {
  progressClass = 'slide-progress-0';
  selectedValue = 0;
  SliderMode = SliderMode;
  sliderValue = 0;

  @Input()
  description: string;

  @Input()
  subDescription: string;

  @Input()
  postFix: string;

  @Input()
  range: number[];

  @Input()
  min: number;

  @Input()
  max: number;

  @Input()
  dialogMax: number;

  @Input()
  step: number;

  @Input()
  toolTipText: string;

  @Input()
  mode: SliderMode;

  @Output()
  onSliderChange = new EventEmitter<any>();

  @Input()
  editModeEnabled: boolean;
  propagateChange = (_: any) => {
  };

  @Output()
  onReset = new EventEmitter<any>();

  @Input()
  resetButtonEnabled: boolean = false;

  HjTooltipPosition = HjTooltipPosition;

  constructor(public dialog: MatDialog) {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(SliderDialogComponent, {
      width: '80%',
      maxWidth: '500px',
      data: {
        description: this.description,
        subDescription: this.subDescription,
        selectedValue: this.selectedValue,
        min: this.min,
        max: this.dialogMax
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.setSliderValue(result);
        this.propagateChange(result);
        this.onSliderChange.emit();
      }
    });
  }

  writeValue(obj: any): void {
    if (obj !== null) {
      this.setSliderValue(obj);
    }
  }

  setSliderValue(value: number) {
    if (SliderMode.SPECIFIC_RANGE === this.mode) {
      this.setSliderToClosestStep(value);
    } else {
      this.sliderValue = value;
    }
    this.updateColorBeforeSelection();
    this.selectedValue = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    // do nothing
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (SliderMode.SPECIFIC_RANGE === this.mode) {
      this.min = 0;
      this.max = this.range.length - 1;
      this.step = 1;
    }
  }

  onResetClick(event: any) {
    this.onReset.emit(this);
  }

  onChange(event: any) {
    this.propagateChanges();
    this.onSliderChange.emit();
    console.log("on change");
    if (this.editModeEnabled && this.selectedValue == this.range[this.range.length - 1]) {
      this.openDialog()
    }
  }

  onSliderModelChange() {
    this.updateColorBeforeSelection();
    if (SliderMode.SPECIFIC_RANGE === this.mode) {
      this.selectedValue = this.range[this.sliderValue];
    } else {
      this.selectedValue = this.sliderValue;
    }
  }

  setSliderToClosestStep(value: number) {
    const closetsValue = this.getClosestNumberInArrayByNumber(this.range, value);
    this.sliderValue = this.range.indexOf(closetsValue);
  }

  getClosestNumberInArrayByNumber(numberArray: number[], number: number) {
    return numberArray.reduce(function (prev, curr) {
      return (Math.abs(curr - number) < Math.abs(prev - number) ? curr : prev);
    });
  }

  propagateChanges() {
    if (SliderMode.SPECIFIC_RANGE === this.mode) {
      this.propagateChange(this.range[this.sliderValue]);
    } else {
      this.propagateChange(this.sliderValue);
    }
  }

  updateColorBeforeSelection() {
    const colorStop = Math.ceil((this.sliderValue - (this.min !== undefined ? this.min : 0)) / (this.max - (this.min !== undefined ? this.min : 0)) * 100);
    this.progressClass = 'slide-progress-' + colorStop;
  }

  toggleTooltip(tooltip: HjTooltipDirective) {
    tooltip.toggleTooltip();
  }
}
