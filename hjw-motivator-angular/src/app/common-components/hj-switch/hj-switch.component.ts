import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from "@angular/forms";

@Component({
  selector: 'hj-switch',
  templateUrl: './hj-switch.component.html',
  styleUrls: ['./hj-switch.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HjSwitchComponent),
      multi: true
    }
  ]
})
export class HjSwitchComponent implements OnInit, ControlValueAccessor {

  constructor() {}

  writeValue(obj: any): void {
    if (obj !== null) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  propagateChange = (_: any) => {
  };

  @Input()
  description: string;
  @Input()
  yesText: string;
  @Input()
  noText: string;
  @Input()
  noMarginBottom: boolean = false;

  value: boolean = false;

  @Output()
  onSwitchChange = new EventEmitter<any>();

  ngOnInit() {
  }

  onClick() {
    this.value = !this.value;
    this.propagateChange(this.value);
    this.onSwitchChange.emit();
  }

  onChange() {
    this.propagateChange(this.value);
    this.onSwitchChange.emit();
  }

  setDisabledState(isDisabled: boolean): void {
  }
}
