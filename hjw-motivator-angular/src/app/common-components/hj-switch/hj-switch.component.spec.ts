import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormsModule} from "@angular/forms";
import {HjSwitchComponent} from "./hj-switch.component";

describe('HjSwitchComponent', () => {
  let component: HjSwitchComponent;
  let fixture: ComponentFixture<HjSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HjSwitchComponent],
      imports: [FormsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HjSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
