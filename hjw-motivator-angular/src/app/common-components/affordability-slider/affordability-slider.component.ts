import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { NG_VALUE_ACCESSOR } from '@angular/forms';


export class AffordabilitySliderChange {
  source: AffordabilitySliderComponent;
  value: number;
  threshold: AffordabilityThreshold;
}

export enum AffordabilityThreshold {
  AFFORDABLE, STRETCHED, AGGRESSIVE
}

export class AffordabilitySliderThresholds {
  affordable: number;
  stretched: number;
}

export class AffordabilitySliderThresholdsLabels {
  affordable: string;
  stretched: string;
  aggressive: string;
}

export const AFFORDABILITY_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  multi: true,
  useExisting: forwardRef(() => AffordabilitySliderComponent)
};

@Component({
  selector: 'hj-affordability-slider',
  templateUrl: './affordability-slider.component.html',
  styleUrls: ['./affordability-slider.component.scss'],
  providers: [AFFORDABILITY_VALUE_ACCESSOR],
})
export class AffordabilitySliderComponent {
  propagateChange = (_: any) => {
  };
  propagateTouched = () => {
  };
  @Output() readonly change: EventEmitter<AffordabilitySliderChange> = new EventEmitter<AffordabilitySliderChange>();
  @Input() disabled = false;
  private _realValue = 0;
  private _activeThreshold: AffordabilityThreshold = AffordabilityThreshold.AFFORDABLE;

  constructor() {
    this.label = this.label.bind(this);

  }

  private _labels: AffordabilitySliderThresholdsLabels;

  get labels(): AffordabilitySliderThresholdsLabels {
    return this._labels;
  }

  @Input()
  set labels(value: AffordabilitySliderThresholdsLabels) {
    this._labels = value;
  }

  private _thresholds: AffordabilitySliderThresholds;

  get thresholds(): AffordabilitySliderThresholds {
    return this._thresholds;
  }

  @Input()
  set thresholds(value: AffordabilitySliderThresholds) {
    this._thresholds = value;
    this._activeThreshold = this._calculateActiveThreshold();
  }

  private _max = 100;

  get max(): number {
    return this._max;
  }

  @Input()
  set max(value: number) {
    this._max = coerceNumberProperty(value, this._max);
  }

  private _min = 0;

  get min(): number {
    return this._min;
  }

  @Input()
  set min(value: number) {
    this._min = coerceNumberProperty(value, this._min);
  }

  private _value = 0;

  get value() {
    return this._value;
  }

  @Input()
  set value(val) {
    this._value = AffordabilitySliderComponent._clamp(coerceNumberProperty(val, this._value), this._min, this.max);
    this._realValue = this._value;
    this._activeThreshold = this._calculateActiveThreshold();
    this._emitChangeEvent();
  }

  get _sliderWrapperClass(): any {
    const cssClasses = {};
    switch (this._activeThreshold) {
      case AffordabilityThreshold.AFFORDABLE:
        cssClasses['affordability-slider-green'] = true;
        break;
      case AffordabilityThreshold.STRETCHED:
        cssClasses['affordability-slider-yellow'] = true;
        break;
      case AffordabilityThreshold.AGGRESSIVE:
        cssClasses['affordability-slider-red'] = true;
        break;
    }
    return cssClasses;
  }

  label() {
    if (!this._labels) {
      return '';
    }
    switch (this._activeThreshold) {
      case AffordabilityThreshold.AFFORDABLE:
        return this._labels.affordable;
      case AffordabilityThreshold.STRETCHED:
        return this._labels.stretched;
      case AffordabilityThreshold.AGGRESSIVE:
        return this._labels.aggressive;
    }
  }

  private static _clamp(val: number, min: number, max: number): number {
    return Math.max(min, Math.min(val, max));
  }

  doChange(event: any) {
    this._realValue = coerceNumberProperty(event.value, this._realValue);
    this._activeThreshold = this._calculateActiveThreshold();
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private _calculateActiveThreshold(): AffordabilityThreshold {
    if (this.thresholds && this.thresholds.stretched && this.thresholds.affordable) {
      if (this._realValue > this.thresholds.stretched) {
        return AffordabilityThreshold.AGGRESSIVE;
      } else if (this._realValue > this.thresholds.affordable) {
        return AffordabilityThreshold.STRETCHED;
      }
    }
    return AffordabilityThreshold.AFFORDABLE;
  }

  private _emitChangeEvent() {
    this.propagateChange(this._value);
    this.change.emit(this._createChangeEvent());
  }

  private _createChangeEvent(): AffordabilitySliderChange {
    const event = new AffordabilitySliderChange();

    event.source = this;
    event.value = this._value;
    event.threshold = this._activeThreshold;

    return event;
  }
}
