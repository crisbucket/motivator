import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AffordabilitySliderComponent} from './affordability-slider.component';
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material.module";

describe('AffordabilitySliderComponent', () => {
  let component: AffordabilitySliderComponent;
  let fixture: ComponentFixture<AffordabilitySliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffordabilitySliderComponent ],
      imports: [ MaterialModule, FormsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffordabilitySliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
