import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CounterComponent} from './counter.component';

describe('CounterComponent', () => {
  let component: CounterComponent;
  let nativeElement: HTMLElement;
  let fixture: ComponentFixture<CounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CounterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CounterComponent);
    component = fixture.componentInstance;
    nativeElement = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('setting value from outside allowed range should default to value form within the range', () => {
    component.min = 0;
    component.max = 10;
    component.value = -10;
    expect(component.value).toBe(0);
    component.value = 100;
    expect(component.value).toBe(10);
  });

  it('value should be visible in span element', () => {
    component.value = 10;
    fixture.detectChanges();
    console.log(fixture.nativeElement.querySelector('#counter-value'));
    console.log(component.value);
    expect(fixture.nativeElement.querySelector('#counter-value').innerHTML).toBe('10');
  });

  it('value should increase after clicking +', () => {
    const incrementButton: HTMLElement = nativeElement.querySelector('#counter-increment');
    component.value = 100;
    fixture.detectChanges();
    expect(component.value).toBe(100);
    incrementButton.click();
    fixture.detectChanges();
    expect(component.value).toBe(101);
  });

  it('value should decrease after clicking -', () => {
    const decrementButton: HTMLElement = nativeElement.querySelector('#counter-decrement');
    component.value = 100;
    fixture.detectChanges();
    expect(component.value).toBe(100);
    decrementButton.click();
    fixture.detectChanges();
    expect(component.value).toBe(99);
  });

  it('value should not increase after clicking + if disabled', () => {
    const incrementButton: HTMLElement = nativeElement.querySelector('#counter-increment');
    component.value = 100;
    component.disabled = true;
    fixture.detectChanges();
    expect(component.value).toBe(100);
    incrementButton.click();
    fixture.detectChanges();
    expect(component.value).toBe(100);
  });

  it('value should not increase after clicking - if disabled', () => {
    const decrementButton: HTMLElement = nativeElement.querySelector('#counter-decrement');
    component.value = 100;
    component.disabled = true;
    fixture.detectChanges();
    expect(component.value).toBe(100);
    decrementButton.click();
    fixture.detectChanges();
    expect(component.value).toBe(100);
  });

  it('element should be blurred after clicking -', () => {
    const decrementButton: HTMLElement = nativeElement.querySelector('#counter-decrement');
    spyOn(component, 'propagateTouched');
    fixture.detectChanges();
    expect(component.propagateTouched).toHaveBeenCalledTimes(0);
    decrementButton.click();
    fixture.detectChanges();
    expect(component.propagateTouched).toHaveBeenCalledTimes(1);
  });

  it('element should be blurred after clicking +', () => {
    const incrementButton: HTMLElement = nativeElement.querySelector('#counter-increment');
    spyOn(component, 'propagateTouched');
    fixture.detectChanges();
    expect(component.propagateTouched).toHaveBeenCalledTimes(0);
    incrementButton.click();
    fixture.detectChanges();
    expect(component.propagateTouched).toHaveBeenCalledTimes(1);
  });

});
