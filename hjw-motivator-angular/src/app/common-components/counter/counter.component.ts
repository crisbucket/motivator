import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'hj-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => CounterComponent),
    }
  ]
})
export class CounterComponent implements OnInit, ControlValueAccessor {
  private _max: number;
  private _min: number;
  private _value = 0;
   propagateChange = (_: any) => {
  }
   propagateTouched = () => {
  }

  @Input()
  disabled = false;

  constructor() {
  }

  get max(): number {
    return this._max;
  }

  @Input()
  set max(value: number) {
    this._max = +value;
  }

  get min(): number {
    return this._min;
  }

  @Input()
  set min(value: number) {
    this._min = +value;
  }

  get value() {
    return this._value;
  }

  @Input()
  set value(val) {
    this._value = this.valueInAllowedRange(+val);
    this.propagateChange(this._value);
  }

  ngOnInit() {
  }

  increment() {
    if (this.disabled) {
      return;
    }
    this.value++;
    this.propagateTouched();
  }

  decrement() {
    if (this.disabled) {
      return;
    }
    this.value--;
    this.propagateTouched();
  }

  writeValue(value: any): void {
    if (value) {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private valueInAllowedRange(val: number): number {
    if (this._min !== undefined) {
      if (val <= this._min) {
        return this._min;
      }
    }
    if (this._max !== undefined) {
      if (val >= this._max) {
        return this._max;
      }
    }
    return val;
  }
}
