import {Injectable} from '@angular/core';
import {environment as env} from '../environments/environment';

@Injectable()
export class AppConstants {

  private BASE_URL =  '';

  constructor() {
    this.BASE_URL = env['API_HOST'];
  }

  get API_HOST() {
    return this.BASE_URL;
  }

  get CREDIT_ASSESSMENT_SERVICE_URL() {
    return (env['API_HOSTS']['creditassessment'] ? env['API_HOSTS']['creditassessment'] : env['API_HOST'] + 'creditassessment/') + 'creditassessment/';
  }

  get FINANCIAL_OVERVIEW_SERVICE_URL() {
    return (env['API_HOSTS']['financialoverview'] ? env['API_HOSTS']['financialoverview'] : env['API_HOST'] + 'financialoverview/') + 'financial-overview/';
  }

  get MODEL_BUDGET_SERVICE_URL() {
    return (env['API_HOSTS']['modelbudget'] ? env['API_HOSTS']['modelbudget'] : env['API_HOST'] + 'modelbudget/') + 'model-budget/';
  }

  get TAXATION_SERVICE_URL() {
    return (env['API_HOSTS']['taxation'] ? env['API_HOSTS']['taxation'] : env['API_HOST'] + 'taxation/') + 'taxation';
  }

  get CHILDREN_SUPPORT_URL() {
    return (env['API_HOSTS']['taxation'] ? env['API_HOSTS']['taxation'] : env['API_HOST'] + 'children-support/') + 'children-support/';
  }

  get MORTGAGE_COMPUTATION_URL () {
    return (env['API_HOSTS']['mortgagecomputation'] ? env['API_HOSTS']['mortgagecomputation'] : env['API_HOST'] + 'ndfmortgageopencomputation/') + 'mortgageproducts';
  }

  get APP_ID() {
    return 'hjnm';
  }
}
