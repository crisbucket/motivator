import {AfterContentInit, Component, ContentChildren, OnInit, QueryList} from '@angular/core';
import {HelpIndicatorDirective} from "../help-indicator.directive";

@Component({
  selector: 'help-container',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit, AfterContentInit {
  ngAfterContentInit(): void {
    console.log(this.indicators);
    this.indicators.forEach(item => console.log(item));
  }

  isVisible = true;

  @ContentChildren(HelpIndicatorDirective, {descendants: true}) indicators: QueryList<HelpIndicatorDirective>;

  ngOnInit() {
  }

  onIsHelpOpen() {
    this.isVisible = !this.isVisible;
  }
}
