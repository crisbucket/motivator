import {AffordabilityThreshold} from "../../../common-components/affordability-slider/affordability-slider.component";

export class CallOutsVisibility {
  realisticCallbackVisible: boolean;
  realisticCallbackWasClosed: boolean;
  unlikelyCallbackVisible: boolean;
  unlikelyCallbackWasClosed: boolean;


  updateAfterClick(activeAffordabilityThreshold : AffordabilityThreshold) {
    if (activeAffordabilityThreshold == AffordabilityThreshold.AFFORDABLE) {
      this.realisticCallbackVisible = !this.realisticCallbackVisible;
      this.realisticCallbackWasClosed = true;
    } else if (activeAffordabilityThreshold == AffordabilityThreshold.STRETCHED) {
      this.unlikelyCallbackVisible = !this.unlikelyCallbackVisible;
      this.unlikelyCallbackWasClosed = true;
    }
  }

  updateWith(activeAffordabilityThreshold : AffordabilityThreshold) {
    if (activeAffordabilityThreshold == AffordabilityThreshold.AFFORDABLE && !this.realisticCallbackWasClosed) {
      this.realisticCallbackVisible = true;
    } else if (activeAffordabilityThreshold == AffordabilityThreshold.STRETCHED && !this.unlikelyCallbackWasClosed) {
      this.unlikelyCallbackVisible = true;
    }
  }
}
