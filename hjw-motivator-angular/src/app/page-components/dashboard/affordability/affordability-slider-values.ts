import {
  AffordabilitySliderThresholds,
  AffordabilitySliderThresholdsLabels
} from "../../../common-components/affordability-slider/affordability-slider.component";
import {ReplaceLabelPipe} from "../../../pipes/replace-label.pipe";
import {CreditAssessment} from "../../../models/credit-assessment/credit-assessment";
import {AffordabilityThresholds} from "../../../models/credit-assessment/affordability-thresholds";

export class AffordabilitySliderValues {
  private replaceLabel: ReplaceLabelPipe;

  min: number;
  max: number;

  thresholds: AffordabilitySliderThresholds;
  labels: AffordabilitySliderThresholdsLabels = new AffordabilitySliderThresholdsLabels();

  constructor(replaceLabel: ReplaceLabelPipe) {
    this.replaceLabel = replaceLabel;
    this.initAffordabilitySliderThresholdsLabels()
  }

  private initAffordabilitySliderThresholdsLabels() {
    this.labels.affordable = this.replaceLabel.transform("affordability.slider.affordable");
    this.labels.stretched = this.replaceLabel.transform("affordability.slider.stretched");
    this.labels.aggressive = this.replaceLabel.transform("affordability.slider.aggressive");
  }

  updateWith(affordabilityThresholds: AffordabilityThresholds) {
    this.min = affordabilityThresholds.affordable-(affordabilityThresholds.max-affordabilityThresholds.affordable);
    this.max = affordabilityThresholds.max;

    this.thresholds = {
      affordable: affordabilityThresholds.affordable,
      stretched: affordabilityThresholds.stretched
    }
  }
}
