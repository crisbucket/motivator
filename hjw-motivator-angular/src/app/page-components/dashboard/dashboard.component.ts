import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FinancialOverviewService} from '../../services/financial-overview.service';
import {CountryCurrency} from "../../models/country-currency";
import {SliderValues} from "../welcome-assessment/slider-values";
import {ReplaceLabelPipe} from "../../pipes/replace-label.pipe";

import * as lodash from 'lodash';
import {DashboardService} from "./dashboard-service";
import {
  AffordabilitySliderChange,
  AffordabilityThreshold
} from "../../common-components/affordability-slider/affordability-slider.component";
import {FinancialOverview} from "../../models/financial-overview/financial-overview";

@Component({
  selector: 'hjwm-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [ReplaceLabelPipe]
})
export class DashboardComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private financialOverviewService: FinancialOverviewService,
              public dashboard: DashboardService) {
  }

  ngOnInit() {
    this.financialOverviewService.createOrLoadSavedFinancialOverview().subscribe(
      financialOverview => {
        this.dashboard.updateFinancialOverview(financialOverview);
      }
    );
  }

  onSliderChange(event: AffordabilitySliderChange) {
    console.log('onSliderChange ');
    this.dashboard.changeAffordableAmount(event.threshold, event.value);
  }

  onHelpIcon() {
    this.dashboard.callbackVisibility.updateAfterClick(this.dashboard.activeAffordabilityThreshold);
  }

  onDashboardMoreDetails() {
    console.log("Loan details and more")
  }

  goNext() {
    console.log(this.financialOverviewService.financialOverview);
    console.log("Apply for a loan promise");
  }


}
