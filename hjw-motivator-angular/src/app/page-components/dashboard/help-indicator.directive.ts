import {Directive, ElementRef} from '@angular/core';

@Directive({
  selector: '[hjHelpIndicator]'
})
export class HelpIndicatorDirective {

  targetElement: HTMLElement = this._el.nativeElement;

  constructor(private _el: ElementRef) {
    console.log(this._el);
  }
}
