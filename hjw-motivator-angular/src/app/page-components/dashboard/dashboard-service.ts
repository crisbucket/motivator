import {Adult} from "../../models/financial-overview/householdsituation/adult";
import {ExpenseGroup} from "../../models/model-budget/expense-group";
import {FinancialOverview} from "../../models/financial-overview/financial-overview";
import {SliderMode} from "../../common-components/models/slider-mode";
import {Country} from "../../models/country";
import {Currency} from "../../models/currency";
import {Injectable} from "@angular/core";
import * as lodash from 'lodash';
import {FinancialOverviewService} from "../../services/financial-overview.service";
import {DebtType} from "../../models/financial-overview/debt-type";
import {Subscription} from "rxjs/Subscription";
import {CreditAssessmentService} from "../../services/credit-assessment.service";
import {MortgageComputationService} from "../../services/mortgage-computation.service";
import {CallOutsVisibility} from "./affordability/call-outs-visibility";
import {AffordabilitySliderValues} from "./affordability/affordability-slider-values";
import {ReplaceLabelPipe} from "../../pipes/replace-label.pipe";
import {AffordabilityThreshold} from "../../common-components/affordability-slider/affordability-slider.component";
import {map} from "rxjs/operators";
import {CountryCurrency} from "../../models/country-currency";
import {SliderValues} from "../welcome-assessment/slider-values";

@Injectable()
export class DashboardService {
  currentlyOpenedElement: any;
  country: Country;
  currency: Currency;
  pendingAffordabilityRequest: Subscription;
  pendingMortgageComputationRequest: Subscription;
  pendingMonthlyPaymentForNonCollateralDebtRequest: Subscription;
  callbackVisibility: CallOutsVisibility;
  affordabilitySliderValues: AffordabilitySliderValues;

  //financial overview
  currentlyEditedFinancialOverview: FinancialOverview;
  initialFinancialOverview: FinancialOverview;

  //adults
  adults: Adult[] = [];
  currentlyEditedAdults: Adult[] = [];
  currentlyEditedDownPayment = 0;
  totalIncome: number;
  downPayment: number;
  downPaymentRange: number[] = [];
  SliderMode = SliderMode;
  taxRange = [];

  //expenses budget breakdown
  expenseGroupPreviousState: ExpenseGroup;
  expenseGroups: ExpenseGroup[] = [];

  //affordability
  activeAffordabilityThreshold: AffordabilityThreshold;

  get activeAffordabilityThresholdTextClass() {
    switch (this.activeAffordabilityThreshold) {
      case AffordabilityThreshold.AFFORDABLE:
        return 'text-affordable';
      case AffordabilityThreshold.STRETCHED:
        return 'text-stretched';
      case AffordabilityThreshold.AGGRESSIVE:
        return 'text-aggressive';
    }
  }
  totalAffordability: number = 0;
  totalLoan: number = 0;
  monthlyLoanCost: number = 0;
  leftForSpending: number = 0;

  constructor(private financialOverviewService: FinancialOverviewService,
              private creditAssessmentService: CreditAssessmentService,
              private mortgageComputationService: MortgageComputationService,
              private replaceLabel: ReplaceLabelPipe) {
    this.callbackVisibility = new CallOutsVisibility();
    this.affordabilitySliderValues = new AffordabilitySliderValues(this.replaceLabel);
  }

  public saveChangesMadeInDashboardIntoFinancialOverviewAndRefreshAffordabilitySlider() {
    this.financialOverviewService.updateFinancialOverview().subscribe(financialOverview => {
      this.updateFinancialOverview(financialOverview);
    });
  }

  public isAnotherElementOpen() {
    return this.currentlyOpenedElement !== undefined;
  }

  public closeAll() {
    this.currentlyOpenedElement = undefined;
  }

  public cancelPendingRequestIfAny() {
    if (this.pendingMonthlyPaymentForNonCollateralDebtRequest) {
      this.pendingMonthlyPaymentForNonCollateralDebtRequest.unsubscribe();
    }
    if (this.pendingAffordabilityRequest) {
      this.pendingAffordabilityRequest.unsubscribe();
    }
    if (this.pendingMortgageComputationRequest) {
      this.pendingMortgageComputationRequest.unsubscribe();
    }
  }

  private updateAffordabilityThresholds(financialOverviewId: string) {
    return this.creditAssessmentService.getAffordabilityThresholds(financialOverviewId)
      .subscribe(affordabilityThresholds => {
        this.affordabilitySliderValues.updateWith(affordabilityThresholds);
        this.activeAffordabilityThreshold = AffordabilityThreshold.AFFORDABLE;
        if (this.totalAffordability !== 0) {
          this.totalAffordability = Math.max(Math.min(this.totalAffordability, affordabilityThresholds.max), affordabilityThresholds.affordable );
        } else {
          this.totalAffordability = affordabilityThresholds.affordable;
        }
        this.updateMonthlyLoanCostForAffordableAmount(affordabilityThresholds.affordable);
      })
  }

  private calculateLeftForSpending() {
    this.leftForSpending = this.totalIncome -
      this.monthlyLoanCost -
      this.currentlyEditedFinancialOverview.totalExpenses -
      this.getOtherMortgage() -
      this.getNcclDebt();
  }

  private getOtherMortgage(): number {
    return this.currentlyEditedFinancialOverview.findDebtByType(DebtType.OTHER_MORTGAGE) != undefined ?
      this.currentlyEditedFinancialOverview.findDebtByType(DebtType.OTHER_MORTGAGE).monthlyPaymentByMarketRate : 0;
  }

  private getNcclDebt(): number {
    return this.currentlyEditedFinancialOverview.findDebtByType(DebtType.NCCL) != undefined ?
      this.currentlyEditedFinancialOverview.findDebtByType(DebtType.NCCL).monthlyPaymentByMarketRate : 0;
  }

  changeAffordableAmount(threshold: AffordabilityThreshold, value: number) {
    console.log('changeAffordableAmount');
    this.activeAffordabilityThreshold = threshold;
    if (this.totalAffordability && value != this.totalAffordability) {
      this.totalAffordability = value;
      this.cancelPendingRequestIfAny();
      this.updateMonthlyLoanCostForAffordableAmount(value - this.initialFinancialOverview.downPayment);
    }
  }

  private updateMonthlyLoanCostForAffordableAmount(affordableAmount: number) {
    console.log('updateMonthlyLoanCostForAffordableAmount');
    return this.mortgageComputationService.getMonthlyPaymentForLoanAmountByMarketRate(affordableAmount - this.initialFinancialOverview.downPayment, this.country)
      .subscribe(monthlyLoanCost => {
          this.totalLoan = affordableAmount - this.initialFinancialOverview.downPayment;
          this.monthlyLoanCost = monthlyLoanCost;
          this.calculateLeftForSpending();
          this.callbackVisibility.updateWith(this.activeAffordabilityThreshold);
        }
      )
  }

  updateFinancialOverview(financialOverview: FinancialOverview) {
    this.cancelPendingRequestIfAny();

    this.currency = CountryCurrency.getByCountry(financialOverview.householdSituation.country);
    this.country = financialOverview.householdSituation.country;

    //budget breakdown
    this.initialFinancialOverview = lodash.cloneDeep(financialOverview);
    this.currentlyEditedFinancialOverview = lodash.cloneDeep(financialOverview);
    this.expenseGroups = lodash.cloneDeep(financialOverview.expenseGroups);

    //adults and childrenSupportIncome
    this.downPayment = financialOverview.downPayment;
    this.adults = financialOverview.householdSituation.adults;
    this.totalIncome = financialOverview.getTotalIncomeAfterTaxWithChildSupport();
    this.downPaymentRange = SliderValues.getDownPaymentRange(financialOverview.householdSituation.country);
    this.taxRange = SliderValues.getIncomeRange(financialOverview.householdSituation.country);

    //update affordability slider
    this.updateAffordabilityThresholds(financialOverview.id);

    this.closeAll();
  }
}
