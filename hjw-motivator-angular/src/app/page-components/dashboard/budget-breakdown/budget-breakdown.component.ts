import {ReplaceLabelPipe} from "../../../pipes/replace-label.pipe";
import {Component, OnInit} from "@angular/core";
import {ExpenseGroup} from "../../../models/model-budget/expense-group";
import * as lodash from 'lodash';
import {FinancialOverviewService} from "../../../services/financial-overview.service";
import {DashboardElementDescriptionService} from "../../../services/dashboard-element-description.service";
import {DashboardService} from "../dashboard-service";

@Component({
  selector: 'budget-breakdown',
  templateUrl: './budget-breakdown.component.html',
  styleUrls: ['./budget-breakdown.component.scss'],
  providers: [ReplaceLabelPipe]
})
export class BudgetBreakdownComponent implements OnInit {
  limit: number = 3;
  sliderMaxFactor :number = 2;

  ngOnInit(): void {
  }

  constructor(private financialOverviewService: FinancialOverviewService,
              private dashboardElementDescriptionService: DashboardElementDescriptionService,
              public dashboard:DashboardService,
              private replaceLabel: ReplaceLabelPipe) {
  }

  openExpenseGroup(expenseGroup: ExpenseGroup) {
    if(this.dashboard.isAnotherElementOpen()) {
      return;
    }
    this.dashboard.expenseGroupPreviousState = lodash.cloneDeep(expenseGroup);
    this.dashboard.closeAll();
    this.dashboard.currentlyOpenedElement = expenseGroup;
  }

  getDescription(expenseGroup: ExpenseGroup): string {
    return this.dashboardElementDescriptionService.getExpenseGroupDescription(expenseGroup, this.dashboard.currentlyEditedFinancialOverview, this.replaceLabel);
  }

  public getSliderMaxValue(groupType: string, expenseName: string): number {
    return this.dashboard.initialFinancialOverview.findExpenseByGroupTypeAndCategory(groupType, expenseName).amount * this.sliderMaxFactor;
  }

  onCancelExpenseGroup(expenseGroup: ExpenseGroup) {
    expenseGroup.expenses = this.dashboard.expenseGroupPreviousState.expenses;
    this.dashboard.closeAll();
  }

  onSaveExpenseGroup(expenseGroup: ExpenseGroup) {
    expenseGroup.totalAmount = Object.assign(new ExpenseGroup(),expenseGroup as ExpenseGroup).getTotalAmount();
    this.financialOverviewService.financialOverview.saveOrUpdateExpenseGroup(expenseGroup);
    this.dashboard.saveChangesMadeInDashboardIntoFinancialOverviewAndRefreshAffordabilitySlider();
  }
}
