import {Component, OnInit} from "@angular/core";
import {ReplaceLabelPipe} from "../../../pipes/replace-label.pipe";
import {Adult} from "../../../models/financial-overview/householdsituation/adult";
import {TaxationService} from "../../../services/taxation.service";
import {FinancialOverviewService} from "../../../services/financial-overview.service";
import * as lodash from 'lodash';
import {DashboardService} from "../dashboard-service";
import {DashboardElementDescriptionService} from "../../../services/dashboard-element-description.service";
import {FamilySituation} from "../../../models/financial-overview/householdsituation/family-situation";

@Component({
  selector: 'income-and-debt',
  templateUrl: './income-and-debt.component.html',
  styleUrls: ['./income-and-debt.component.scss'],
  providers: [ReplaceLabelPipe]
})
export class IncomeAndDebtComponent implements OnInit {
  manualTaxSetting :boolean = true;

  constructor(private taxationService: TaxationService,
              private financialOverviewService: FinancialOverviewService,
              public dashboard: DashboardService,
              private dashboardElementDescriptionService: DashboardElementDescriptionService,
              private replaceLabel: ReplaceLabelPipe
  ) {
  }

  ngOnInit(): void {
  }

  adultHasManualTaxSetting(adultIndex: number) {
   if(this.manualTaxSetting[adultIndex] != true) { //can be also undefined
     return false;
   }
    return this.manualTaxSetting[adultIndex];
  }

  getDescription(familySituation: FamilySituation): string {
    return this.dashboardElementDescriptionService.getFamilySituationDescription(familySituation, this.replaceLabel);
  }

  onIncomeOpen() {
    if(this.dashboard.isAnotherElementOpen()) {
      return;
    }
    this.dashboard.currentlyEditedAdults = lodash.cloneDeep(this.dashboard.adults);
    this.dashboard.currentlyOpenedElement = this.dashboard.currentlyEditedAdults;
  }

  onDownPaymentOpen() {
    if(this.dashboard.isAnotherElementOpen()) {
      return;
    }
    this.dashboard.currentlyEditedDownPayment = lodash.cloneDeep(this.dashboard.downPayment);
    this.dashboard.currentlyOpenedElement = this.dashboard.downPayment;
  }

  onDeleteAdult() {
    if (this.dashboard.currentlyEditedAdults.length == 2) {
      this.dashboard.currentlyEditedAdults.pop();
    }
  }

  onAddAdult() {
    if (this.dashboard.currentlyEditedAdults.length == 1) {
      const adult = new Adult();
      this.dashboard.currentlyEditedAdults.push(adult);
    }
  }

  recalculateIncomeAfterTax(adult) {
    this.setIncomeAfterTaxValueAutomatic(adult);
  }

  private setIncomeAfterTaxValueAutomatic(adult) {
    this.taxationService.getIncomeAfterTaxation(adult.incomeBeforeTaxation, this.financialOverviewService.financialOverview.householdSituation.country)
      .subscribe(value => adult.incomeAfterTaxation = value.amount);
  }

  onSaveIncome() {
    this.dashboard.adults = this.dashboard.currentlyEditedAdults;
    this.financialOverviewService.financialOverview.updateAdults(this.dashboard.currentlyEditedAdults);
    this.dashboard.saveChangesMadeInDashboardIntoFinancialOverviewAndRefreshAffordabilitySlider();
  }

  onCancelIncome() {
    this.dashboard.currentlyEditedAdults = lodash.cloneDeep(this.financialOverviewService.financialOverview.householdSituation.adults);
    this.dashboard.closeAll();
  }

  onSaveDownPayment() {
    this.dashboard.downPayment = this.dashboard.currentlyEditedDownPayment;
    this.financialOverviewService.financialOverview.downPayment = this.dashboard.currentlyEditedDownPayment;
    this.dashboard.saveChangesMadeInDashboardIntoFinancialOverviewAndRefreshAffordabilitySlider();
  }

  onCancelDownPayment() {
    this.dashboard.currentlyEditedDownPayment = lodash.cloneDeep(this.dashboard.downPayment);
    this.dashboard.closeAll();
  }
}
