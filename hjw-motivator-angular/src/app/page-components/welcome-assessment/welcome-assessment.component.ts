import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FinancialOverviewService} from '../../services/financial-overview.service';
import {CreditAssessmentService} from '../../services/credit-assessment.service';
import {CreditAssessment} from '../../models/credit-assessment/credit-assessment';
import {CreditAssessmentRequest} from '../../models/credit-assessment/credit-assessment-request';
import {Subscription} from 'rxjs/Subscription';
import {SliderMode} from '../../common-components/models/slider-mode';
import {MortgageComputationService} from '../../services/mortgage-computation.service';
import {SliderValues} from './slider-values';
import {Country} from '../../models/country';
import {Currency} from '../../models/currency';
import {CountryCurrency} from '../../models/country-currency';
import {PageStoreService} from '../../services/page-store.service';
import {HjTooltipType} from '../../common-components/tooltip/tooltip-container/hj-tooltip-container.component';
import {NumberAmountPipe} from '../../pipes/number-amount.pipe';
import {ReplaceLabelPipe} from '../../pipes/replace-label.pipe';
import {FormatPipe} from '../../pipes/format.pipe';
import {PercentPipe} from '@angular/common';


@Component({
  selector: 'hjwm-welcome-assessment',
  templateUrl: './welcome-assessment.component.html',
  styleUrls: ['./welcome-assessment.component.scss'],
  providers: [
    ReplaceLabelPipe,
    NumberAmountPipe,
    FormatPipe,
    PercentPipe
  ]
})
export class WelcomeAssessmentComponent implements OnInit {

  // should come from service or something
  country: Country = Country.DK;
  currency: Currency;
  householdIncomeRange: number[];
  downPaymentRange: number[];
  totalDebtRange: number[];
  maxIncome: number;
  maxDownPayment: number;
  maxDebt: number;

  SliderMode = SliderMode;
  creditAssessment: CreditAssessment = new CreditAssessment();
  monthlyLoanCost = 0;
  hideLimitedByDownPaymentCallback = false;
  mainTooltip: { text: string, visible: boolean, enabled: boolean, type: HjTooltipType } = {
    text: '',
    visible: false,
    enabled: false,
    type: HjTooltipType.WARNING
  };
  private pendingCreditAssessmentRequest: Subscription;
  private pendingMortgageComputationRequest: Subscription;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private financialOverviewService: FinancialOverviewService,
              private creditAssessmentService: CreditAssessmentService,
              private mortgageComputationService: MortgageComputationService,
              public store: PageStoreService,
              private replaceLabelPipe: ReplaceLabelPipe,
              private numberPipe: NumberAmountPipe,
              private formatPipe: FormatPipe,
              private percentPipe: PercentPipe) {
  }

  private get creditAssessmentRequest(): CreditAssessmentRequest {
    return new CreditAssessmentRequest(
      this.store.welcomeAssessment.householdIncomeValue * 12,
      this.store.welcomeAssessment.totalDebtValue,
      this.store.welcomeAssessment.downPaymentValue,
      this.country);
  }

  ngOnInit() {
    this.financialOverviewService.clearFinancialOverviewId();
    this.updateMainTooltip();
    this.store.cleanStores();
    this.country = Country[<string>this.activatedRoute.snapshot.queryParams["country"]] || Country.DK;
    this.currency = CountryCurrency.getByCountry(this.country);
    this.householdIncomeRange = SliderValues.getIncomeRange(this.country);
    this.downPaymentRange = SliderValues.getDownPaymentRange(this.country);
    this.totalDebtRange = SliderValues.getTotalDebtRange(this.country);
    this.maxIncome = SliderValues.getMaxIncome(this.country);
    this.maxDownPayment = SliderValues.getMaxDownPayment(this.country);
    this.maxDebt = SliderValues.getMaxDebt(this.country);
  }

  goNext() {
    this.updateFinancialOverview();
    this.router.navigate(['adults']);
  }

  onSliderChange() {
    if (this.areIncomeAndDownPaymentValid()) {
      this.getCreditAssessment();
    } else {
      this.creditAssessment = new CreditAssessment();
      this.updateMainTooltip();
      console.log(this.store.welcomeAssessment);
    }
  }

  getCreditAssessment() {
    this.cancelPendingRequests();
    this.pendingCreditAssessmentRequest = this.creditAssessmentService
      .getCreditAssessmentOnDTI(this.creditAssessmentRequest)
      .subscribe(value => {
        this.creditAssessment = value;
        this.getMonthlyPaymentForLoanAmount();
        this.updateMainTooltip();
      });
  }

  areIncomeAndDownPaymentValid() {
    return this.store.welcomeAssessment.householdIncomeValue > 0 && this.store.welcomeAssessment.downPaymentValue > 0;
  }

  isLimitedByDownPayment() {
    return this.store.welcomeAssessment.downPaymentValue < this.creditAssessment.minDownPaymentForDtiAffordability;
  }

  onHelpIconClicked() {
    if (this.mainTooltip.enabled) {
      this.mainTooltip.visible = !this.mainTooltip.visible;
      if (this.isLimitedByDownPaymentCallbackVisible()) {
        this.hideLimitedByDownPaymentCallback = true;
      }
    } else if (this.isLimitedByDownPayment()) {
      this.hideLimitedByDownPaymentCallback = false;
      this.updateMainTooltip();
    }
  }

  isLimitedByDownPaymentCallbackVisible() {
    return !this.hideLimitedByDownPaymentCallback && this.areIncomeAndDownPaymentValid() && this.isLimitedByDownPayment();
  }

  private updateMainTooltip() {
    if (!this.areIncomeAndDownPaymentValid()) {
      this.showNecessaryFieldsWarning();
    } else if (this.isLimitedByDownPaymentCallbackVisible()) {
      this.showLimitedByDownPaymentInformation();
    } else {
      this.hideToolTip();
    }
  }

  private hideToolTip() {
    this.mainTooltip = {...this.mainTooltip, text: '', visible: false, enabled: false};
  }

  private showLimitedByDownPaymentInformation() {
    let text = this.replaceLabelPipe.transform('downPaymentPurchaseLimitDescription');
    text = this.formatPipe
      .transform(text,
        this.numberPipe.transform(this.creditAssessment.totalAffordabilityOnDti),
        this.replaceLabelPipe.transform('currency' + this.currency),
        this.numberPipe.transform(this.creditAssessment.minDownPaymentForDtiAffordability),
        this.percentPipe.transform(this.creditAssessment.downPaymentLimit));
    this.mainTooltip = {...this.mainTooltip, text: text, visible: true, enabled: true, type: HjTooltipType.INFORMATION};
  }

  private showNecessaryFieldsWarning() {
    const text = this.replaceLabelPipe.transform('indicativePurchaseLimitDescription');
    this.mainTooltip = {...this.mainTooltip, text: text, visible: true, enabled: true, type: HjTooltipType.WARNING};
  }

  private updateFinancialOverview() {
    this.financialOverviewService.financialOverview.householdSituation.country = this.country;
    this.financialOverviewService.financialOverview.totalDebt = this.store.welcomeAssessment.totalDebtValue;
    this.financialOverviewService.financialOverview.setDownPayment(this.store.welcomeAssessment.downPaymentValue);
    this.financialOverviewService.financialOverview.setTotalAffordability(this.creditAssessment.totalAffordability);
    this.financialOverviewService.financialOverview.setLoanAmount(this.creditAssessment.totalLoan);
    this.financialOverviewService.financialOverview.setMonthlyLoanCost(this.monthlyLoanCost);
  }

  private getMonthlyPaymentForLoanAmount() {
    this.pendingMortgageComputationRequest = this.mortgageComputationService
      .getMonthlyPaymentForLoanAmountByMarketRate(this.creditAssessment.totalAffordability, this.country)
      .subscribe(value => {
        this.monthlyLoanCost = value;
      });
  }

  private cancelPendingRequests() {
    this.cancelPendingCreditAssessmentRequestIfAny();
    this.cancelPendingMortgageComputationRequestIfAny();
  }

  private cancelPendingCreditAssessmentRequestIfAny() {
    if (this.pendingCreditAssessmentRequest) {
      this.pendingCreditAssessmentRequest.unsubscribe();
    }
  }

  private cancelPendingMortgageComputationRequestIfAny() {
    if (this.pendingMortgageComputationRequest) {
      this.pendingMortgageComputationRequest.unsubscribe();
    }
  }
}
