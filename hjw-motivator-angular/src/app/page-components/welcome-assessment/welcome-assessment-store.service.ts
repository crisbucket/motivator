import {CleaneablePageStore} from "../cleanable-page-store";

export class WelcomeAssessmentStore implements CleaneablePageStore {
  householdIncomeValue = 0;
  downPaymentValue = 0;
  totalDebtValue = 0;

  clean() {
    this.householdIncomeValue = 0;
    this.downPaymentValue = 0;
    this.totalDebtValue = 0;
  }

}
