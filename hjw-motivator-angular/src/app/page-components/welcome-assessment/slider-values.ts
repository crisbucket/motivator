import {Country} from "../../models/country";

interface RangedIterable extends Iterable < {} > {
  length: number;
}

class RangedArray < T > extends Array < T > {
  static range(startValue: number, firstStep: number, increment: number, to: number): number[] {
    let currentLenght = Math.floor((to - firstStep) / increment) + 1;
    let result = Array.from(
      ( < RangedIterable > { length: currentLenght }),
      (v, k) => firstStep + k * increment
    );
    if (currentLenght > 0 && result[currentLenght - 1] != to) {
      result.push(to);
    }
    result.unshift(startValue);
    return result;
  }
}

export class SliderValues {
  static getIncomeRange(country: Country): number[] {
    switch (country) {
      case Country.SE:
      case Country.DK:
      case Country.NO:
        return RangedArray.range(0, 10000, 1000, 80000);
      case Country.FI:
        return RangedArray.range(0, 2000, 250, 15000);
    }
  }

  static getIncomeRangeAfterTax(country: Country): number[] {
    switch (country) {
      case Country.SE:
      case Country.DK:
      case Country.NO:
        return RangedArray.range(0, 5000, 500, 50000);
      case Country.FI:
        return RangedArray.range(0, 500, 50, 7000);
    }
  }

  static getDownPaymentRange(country: Country) {
    switch(country) {
      case Country.DK:
        return RangedArray.range(0, 25000, 5000, 400000);
      case Country.NO:
        return RangedArray.range(0, 30000, 15000, 900000);
      case Country.FI:
        return RangedArray.range(0, 4000, 1000, 450000);
      case Country.SE:
        return RangedArray.range(0, 30000, 15000, 900000);
    }
  }

  static getTotalDebtRange(country: Country) {
    switch(country) {
      case Country.DK:
      case Country.SE:
      case Country.NO:
        return RangedArray.range(0, 0, 5000, 500000);
      case Country.FI:
        return RangedArray.range(0, 0, 1000, 70000);
    }
  }

  static getMaxIncome(country: Country): number {
    switch (country) {
      case Country.DK:
      case Country.NO:
      case Country.SE:
        return 250000;
      case Country.FI:
        return 40000;
    }
  }

  static getMaxIncomeAfterTax(country: Country): number {
    switch (country) {
      case Country.DK:
      case Country.NO:
      case Country.SE:
        return 150000;
      case Country.FI:
        return 25000;
    }
  }

  static getMaxDownPayment(country: Country): number {
    switch (country) {
      case Country.DK:
      case Country.SE:
        return undefined;
      case Country.NO:
        return 10000000;
      case Country.FI:
        return 150000;
    }
  }

  static getMaxDebt(country: Country): number {
    switch (country) {
      case Country.DK:
      case Country.SE:
        return undefined;
      case Country.NO:
        return 10000000;
      case Country.FI:
        return 150000;
    }
  }
}
