import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {WelcomeAssessmentComponent} from './welcome-assessment.component';
import {CreditAssessmentService} from "../../services/credit-assessment.service";
import {MaterialModule} from "../../material.module";
import {FormsModule} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {FinancialOverviewService} from "../../services/financial-overview.service";
import {MortgageComputationService} from "../../services/mortgage-computation.service";
import {PipesMockModule} from "../../pipes/pipes.mock.module";
import {FormatPipe} from "../../pipes/format.pipe";
import {HjValueSliderComponent} from "../../common-components/hj-value-slider/hj-value-slider.component";
import {MockReplaceLabelPipe} from "../../pipes/replace-label.pipe.mock";
import {ReplaceLabelPipe} from "../../pipes/replace-label.pipe";
import {PageStoreService} from "../../services/page-store.service";
import {CreditAssessment} from "../../models/credit-assessment/credit-assessment";
import {CreditAssessmentRequest} from "../../models/credit-assessment/credit-assessment-request";
import {Country} from "../../models/country";
import {asyncData} from "../../testing/async-observable-helpers";
import {TooltipModule} from "../../common-components/tooltip/tooltip.module";

describe('WelcomeAssessmentComponent', () => {
  let component: WelcomeAssessmentComponent;
  let fixture: ComponentFixture<WelcomeAssessmentComponent>;

  const creditAssessmentServiceSpy = jasmine.createSpyObj('CreditAssessmentService', ['getCreditAssessmentOnDTI']);
  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const financialOverviewSpy = jasmine.createSpyObj('FinancialOverviewService', ['getFinancialOverview', 'clearFinancialOverviewId']);
  const mortgageComputationServiceSpy = jasmine.createSpyObj("MortgageComputationService", ['getMonthlyPaymentForLoanAmountByMarketRate']);

  beforeEach(async(() => {
    TestBed.overrideProvider(ReplaceLabelPipe, {useValue: new MockReplaceLabelPipe()});
    TestBed.configureTestingModule({
      declarations: [ WelcomeAssessmentComponent, FormatPipe, HjValueSliderComponent ],
      providers: [
        { provide: ActivatedRoute, useValue: {snapshot: {queryParams: {}}}},
        { provide: Router, useValue: routerSpy },
        { provide: CreditAssessmentService, useValue: creditAssessmentServiceSpy },
        { provide: FinancialOverviewService, useValue: financialOverviewSpy },
        { provide: MortgageComputationService, useValue: mortgageComputationServiceSpy },
        { provide: PageStoreService, useClass: PageStoreService},
      ],
      imports: [ MaterialModule, FormsModule, PipesMockModule, TooltipModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeAssessmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onSliderChange should get CreditAssessment from the service and update assessment data', async(() => {
    let creditAssessment = new CreditAssessment();
    creditAssessmentServiceSpy.getCreditAssessmentOnDTI.and.returnValue(asyncData(creditAssessment));
    mortgageComputationServiceSpy.getMonthlyPaymentForLoanAmountByMarketRate.and.returnValue(asyncData(5));
    component.store.welcomeAssessment.householdIncomeValue = 1;
    component.store.welcomeAssessment.downPaymentValue = 1;
    component.onSliderChange();
    let expectedCreditAssessmentRequest = new CreditAssessmentRequest(12, 0, 1, Country.DK);
    expect(creditAssessmentServiceSpy.getCreditAssessmentOnDTI.calls.first().args[0]).toEqual(expectedCreditAssessmentRequest);
    fixture.whenStable().then(() => {
      expect(component.creditAssessment).toEqual(creditAssessment);
    })
  }));
});
