import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CommonComponentsModule} from "../common-components/common-components.module";
import {FormsModule} from "@angular/forms";
import {WelcomeAssessmentComponent} from "./welcome-assessment/welcome-assessment.component";
import {AdultsAndIncomeComponent} from "./adults-and-income/adults-and-income.component";
import {PipesModule} from "../pipes/pipes.module";
import {HelpIndicatorDirective} from './dashboard/help-indicator.directive';
import {HelpComponent} from "./dashboard/help-overlay-container/help.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {PropertyComponent} from "./property/property.component";
import {ChildrenComponent} from 'src/app/page-components/children/children.component';
import {CarsComponent} from "./cars/cars.component";
import {IncomeAndDebtComponent} from "./dashboard/income-and-debt/income-and-debt.component";
import {BudgetBreakdownComponent} from "./dashboard/budget-breakdown/budget-breakdown.component";

@NgModule({
  imports: [
    CommonModule,
    CommonComponentsModule,
    FormsModule,
    PipesModule
  ],
  declarations: [
    WelcomeAssessmentComponent,
    AdultsAndIncomeComponent,
    HelpComponent,
    HelpIndicatorDirective,
    DashboardComponent,
    IncomeAndDebtComponent,
    BudgetBreakdownComponent,
    ChildrenComponent,
    CarsComponent,
    PropertyComponent,
  ],
  exports: [
    WelcomeAssessmentComponent,
    AdultsAndIncomeComponent,
    HelpComponent,
    HelpIndicatorDirective,
    DashboardComponent,
    IncomeAndDebtComponent,
    BudgetBreakdownComponent,
    ChildrenComponent,
    CarsComponent,
    PropertyComponent,
  ]
})
export class PageComponentsModule {
}
