import { PageComponentsModule } from './page-components.module';

describe('PageComponentsModule', () => {
  let pageComponentsModule: PageComponentsModule;

  beforeEach(() => {
    pageComponentsModule = new PageComponentsModule();
  });

  it('should create an instance', () => {
    expect(pageComponentsModule).toBeTruthy();
  });
});
