import {CleaneablePageStore} from "../cleanable-page-store";

export class PropertyStore implements CleaneablePageStore {
  operationalCost = 0;
  loanLeft = 0;
  isExistingProperty = false;

  clean() {
    this.operationalCost = 0;
    this.loanLeft = 0;
    this.isExistingProperty = false;
  }
}
