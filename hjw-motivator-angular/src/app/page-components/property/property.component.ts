import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FinancialOverviewService} from '../../services/financial-overview.service';
import {Debt} from '../../models/financial-overview/debt';
import {ExpenseGroup} from '../../models/model-budget/expense-group';
import {Expense} from '../../models/model-budget/expense';
import {DebtType} from "../../models/financial-overview/debt-type";
import {PropertyStore} from "./property-store";
import {PageStoreService} from "../../services/page-store.service";
import {CountryCurrency} from "../../models/country-currency";
import {Currency} from "../../models/currency";

@Component({
  selector: 'hjwm-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class PropertyComponent implements OnInit {
  private propertyStore: PropertyStore;
  private currency: Currency;

  constructor(private router: Router,
              private financialOverviewService: FinancialOverviewService,
              public store: PageStoreService) {
  }

  ngOnInit() {
    this.onExistingPropertyChange = this.onExistingPropertyChange.bind(this);
    let country = this.financialOverviewService.financialOverview.householdSituation.country;
    this.currency = CountryCurrency.getByCountry(country);
  }

  onSlide() {
    this.store.property.isExistingProperty = !this.store.property.isExistingProperty;
  }

  onExistingPropertyChange() {
    if (!this.store.property.isExistingProperty) {
      this.store.property.operationalCost = 0;
      this.store.property.loanLeft = 0;
    }
  }

  goBack() {
    this.router.navigate(['cars']);
  }

  goToStart() {
    this.router.navigate(['']);
  }

  goNext() {
    this.financialOverviewService.financialOverview.saveOrUpdateExpenseGroup(this.createHousingExpenseGroupWithOperationalCost());
    this.financialOverviewService.financialOverview.saveOrUpdateDebt(new Debt(DebtType.OTHER_MORTGAGE, this.store.property.loanLeft));
    this.calculateNcclDebt();
    this.router.navigate(['dashboard']);
  }

  private calculateNcclDebt() {
    let ncclDebtAmount = this.financialOverviewService.financialOverview.totalDebt - this.store.property.loanLeft;
    this.financialOverviewService.financialOverview.saveOrUpdateDebt(new Debt(DebtType.NCCL, ncclDebtAmount))
  }

  createHousingExpenseGroupWithOperationalCost(): ExpenseGroup {
    const expenseGroup = new ExpenseGroup();
    expenseGroup.groupType = 'EXISTING_PROPERTIES';
    const expense = new Expense();
    expense.category = 'OPERATIONAL_COST';
    expense.amount = this.store.property.operationalCost;
    expenseGroup.expenses.push(expense);
    return expenseGroup;
  }
}
