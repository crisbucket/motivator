import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PropertyComponent} from './property.component';
import {MaterialModule} from "../../material.module";
import {FormsModule} from "@angular/forms";
import {CommonComponentsModule} from "../../common-components/common-components.module";
import {RouterTestingModule} from "@angular/router/testing";
import {FinancialOverviewService} from "../../services/financial-overview.service";
import {PipesMockModule} from "../../pipes/pipes.mock.module";
import {PageStoreService} from "../../services/page-store.service";
import {FinancialOverview} from "../../models/financial-overview/financial-overview";

describe('PropertyComponent', () => {
  let component: PropertyComponent;
  let fixture: ComponentFixture<PropertyComponent>;
  const financialOverviewSpy = new FinancialOverviewService(null, null, null, null);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyComponent ],
      providers: [
        { provide: FinancialOverviewService, useValue: financialOverviewSpy},
        { provide: PageStoreService, useClass: PageStoreService},
      ],
      imports: [MaterialModule, FormsModule, CommonComponentsModule, PipesMockModule,
        RouterTestingModule.withRoutes([])]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    spyOnProperty(financialOverviewSpy, 'financialOverview', 'get').and.returnValue(new FinancialOverview());
    fixture = TestBed.createComponent(PropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
