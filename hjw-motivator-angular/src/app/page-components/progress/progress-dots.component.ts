import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'progress-dots',
  templateUrl: './progress-dots.component.html',
  styleUrls: ['./progress-dots.component.scss'],
})
export class ProgressDotsComponent implements OnInit {
  public progressTrackingRoutes = ['adults', 'children', 'cars', 'property'];

  constructor(private router: Router) {
  }

  ngOnInit() {}

  isCurrentRoute(routeName: string) {
    return this.router.isActive(routeName, true);
  }
}
