import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FinancialOverviewService} from '../../services/financial-overview.service';
import {SliderMode} from "../../common-components/models/slider-mode";
import {Currency} from "../../models/currency";
import {CountryCurrency} from "../../models/country-currency";
import {ComponentConfiguration, CounterConfiguration} from "./component-configuration";
import {PageStoreService} from "../../services/page-store.service";

@Component({
  selector: 'hjwm-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {
  currency: Currency;

  otherExpensesForTransportationRange: number[];
  counterConfiguration: CounterConfiguration;
  SliderMode = SliderMode;

  constructor(private router: Router,
              private financialOverviewService: FinancialOverviewService,
              public store: PageStoreService
  ) {
  }

  ngOnInit() {
    this.onChange = this.onChange.bind(this);
    this.initByCountry();
    this.store.cars.numberOfCars = this.financialOverviewService.financialOverview.householdSituation.numberOfCars;
    this.store.cars.otherTransportationCost = this.financialOverviewService.financialOverview.householdSituation.otherTransportationCost;
    if (this.store.cars.otherTransportationCost > 0) {
      this.store.cars.otherTransportExpensesAreEnabled = true;
    }
  }

  private initByCountry() {
    let country = this.financialOverviewService.financialOverview.householdSituation.country;
    this.currency = CountryCurrency.getByCountry(country);
    this.otherExpensesForTransportationRange = ComponentConfiguration.getSliderConfiguration(country);
    this.counterConfiguration = ComponentConfiguration.getCounterConfiguration(country);
  }

  goNext() {
    this.financialOverviewService.financialOverview.householdSituation.numberOfCars = this.store.cars.numberOfCars;
    this.financialOverviewService.financialOverview.householdSituation.otherTransportationCost = this.store.cars.otherTransportationCost;
    this.router.navigate(['property']);
  }

  goBack() {
    this.router.navigate(['children']);
  }

  goToStart() {
    this.router.navigate(['']);
  }

  onChange() {
    if (!this.store.cars.otherTransportExpensesAreEnabled) {
      this.store.cars.otherTransportationCost = 0;
    }
  }
}
