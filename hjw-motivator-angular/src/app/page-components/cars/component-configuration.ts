import {Country} from "../../models/country";

interface RangedIterable extends Iterable <{}> {
  length: number;
}

class RangedArray<T> extends Array <T> {
  static range(from: number, to: number, step: number): number[] {
    let currentLenght = Math.floor((to - from) / step) + 1;
    let result = Array.from(
      (< RangedIterable > {length: currentLenght}),
      (v, k) => from + k * step
    );
    if (currentLenght > 0 && result[currentLenght - 1] != to) {
      result.push(to);
    }
    return result;
  }
}

export class ComponentConfiguration {
  static getSliderConfiguration(country: Country): number[] {
    switch (country) {
      case Country.DK:
        return RangedArray.range(0, 5000, 100);
      case Country.NO:
        return RangedArray.range(0, 5000, 100);
      case Country.SE:
        return RangedArray.range(0, 5000, 100);
      case Country.FI:
        return RangedArray.range(0, 750, 15);
    }
  }

  static getCounterConfiguration(country: Country): CounterConfiguration {
    switch (country) {
      case Country.DK:
        return new CounterConfiguration(0, 10);
      case Country.NO:
        return new CounterConfiguration(0, 10);
      case Country.SE:
        return new CounterConfiguration(0, 10);
      case Country.FI:
        return new CounterConfiguration(0, 10);
    }
  }
}

export class CounterConfiguration {
  min: number;
  max: number;

  constructor(min: number, max: number) {
    this.min = min;
    this.max = max;
  }
}
