import {CleaneablePageStore} from "../cleanable-page-store";

export class CarsStore implements CleaneablePageStore {
  otherTransportExpensesAreEnabled: boolean;
  numberOfCars: number;
  otherTransportationCost: number;

  clean() {
    this.otherTransportExpensesAreEnabled = false;
    this.numberOfCars = 0;
    this.otherTransportationCost = 0;
  }
}
