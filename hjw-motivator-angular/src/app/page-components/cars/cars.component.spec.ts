import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CarsComponent} from './cars.component';
import {CommonComponentsModule} from "../../common-components/common-components.module";
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material.module";
import {RouterTestingModule} from "@angular/router/testing";
import {FinancialOverviewService} from "../../services/financial-overview.service";
import {HouseholdSituation} from "../../models/financial-overview/householdsituation/household-situation";
import {FinancialOverview} from "../../models/financial-overview/financial-overview";
import {PipesMockModule} from "../../pipes/pipes.mock.module";
import {PageStoreService} from "../../services/page-store.service";

describe('CarsComponent', () => {
  let component: CarsComponent;
  let fixture: ComponentFixture<CarsComponent>;
  const financialOverviewSpy = new FinancialOverviewService(null, null, null, null);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarsComponent ],
      providers: [
        { provide: FinancialOverviewService, useValue: financialOverviewSpy },
        { provide: PageStoreService, useClass: PageStoreService},
      ],
      imports: [MaterialModule, FormsModule, CommonComponentsModule, PipesMockModule,
        RouterTestingModule.withRoutes([])
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    let financialOverview = new FinancialOverview();
    financialOverview.householdSituation = new HouseholdSituation();
    financialOverview.householdSituation.numberOfCars = 1;
    spyOnProperty(financialOverviewSpy, 'financialOverview', 'get').and.returnValue(financialOverview);
    fixture = TestBed.createComponent(CarsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
