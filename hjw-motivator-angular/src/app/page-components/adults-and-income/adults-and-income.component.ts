import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FinancialOverviewService} from '../../services/financial-overview.service';
import {TaxationService} from '../../services/taxation.service';
import {SliderMode} from '../../common-components/models/slider-mode';
import {PageStoreService} from "../../services/page-store.service";
import {SliderValues} from "../welcome-assessment/slider-values";
import {Currency} from "../../models/currency";
import {CountryCurrency} from "../../models/country-currency";
import {Country} from "../../models/country";
import {AdultsAndIncomeStore} from "./adults-and-income-store";
import {Adult} from "../../models/financial-overview/householdsituation/adult";

@Component({
  selector: 'hjwm-adults-and-income',
  templateUrl: './adults-and-income.component.html',
  styleUrls: ['./adults-and-income.component.scss']
})
export class AdultsAndIncomeComponent implements OnInit {
  SliderMode = SliderMode;
  incomeRangeBeforeTax: number[] = [];
  incomeRangeAfterTax: number[] = [];
  country: Country;
  currency: Currency;

  maxIncomeBeforeTax: number;
  maxIncomeAfterTax: number;

  adultsAndIncome: AdultsAndIncomeStore;

  constructor(private router: Router,
              private financialOverviewService: FinancialOverviewService,
              private taxationService: TaxationService,
              public store: PageStoreService) {
  }

  ngOnInit() {
    this.country = this.financialOverviewService.financialOverview.householdSituation.country;
    this.currency = CountryCurrency.getByCountry(this.country);
    this.incomeRangeBeforeTax = SliderValues.getIncomeRange(this.country);
    this.incomeRangeAfterTax = SliderValues.getIncomeRangeAfterTax(this.country);
    this.maxIncomeBeforeTax = SliderValues.getMaxIncome(this.country);
    this.maxIncomeAfterTax = SliderValues.getMaxIncomeAfterTax(this.country);
    this.adultsAndIncome = this.store.adultsAndIncome;
    this.adultsAndIncome.manualIncomeAfterTax = this.isFinishCountry();
    if (this.adultsAndIncome.adults.length == 0) {
      this.adultsAndIncome.adults.push(new Adult());
      this.setDefaultIncomeValues();
    }
  }

  onAdultCountChanged(event) {
    const adultCount: number = Number(this.adultsAndIncome.adultCount);
    if (this.adultsAndIncome.adults.length >= adultCount) {
      this.removeAdult();
    } else {
      this.addAdult();
    }
    this.setDefaultIncomeValues();
  }

  private addAdult() {
    this.adultsAndIncome.adults.push(new Adult());
    if (this.adultsAndIncome.manualIncomeAfterTax) {
      this.adultsAndIncome.distributeAmountAfterTax();
    }
  }

  private removeAdult() {
    if (this.adultsAndIncome.manualIncomeAfterTax) {
      this.adultsAndIncome.collateIncomeAfterTaxOnFirstAdult();
    }
    this.adultsAndIncome.adults.pop();
  }

  onIncomeAfterTaxSwitchChanged() {
    if (this.adultsAndIncome.manualIncomeAfterTax) {
      this.adultsAndIncome.storeTempIncomesBeforeTaxation();
    } else {
      this.adultsAndIncome.restoreIncomesBeforeTaxationFromTemp();
      this.setIncomeAfterTaxAutomaticForAllAdults();
    }
  }

  private setIncomeAfterTaxAutomaticForAllAdults() {
    this.adultsAndIncome.adults.forEach(adult => this.setIncomeAfterTaxValueAutomatic(adult));
  }

  private setDefaultIncomeValues() {
    this.setDefaultIncomeBeforeTaxation();
    this.setDefaultIncomeAfterTaxation();
  }

  private setDefaultIncomeAfterTaxation() {
    if (!this.adultsAndIncome.manualIncomeAfterTax) {
      this.setIncomeAfterTaxAutomaticForAllAdults();
    } else if (this.isFinishCountry()) {
      let income = this.getHouseholdIncomePerAdult();
      this.adultsAndIncome.adults.forEach(adult => adult.incomeAfterTaxation = income);
    }
  }

  private setDefaultIncomeBeforeTaxation() {
    let income = this.getHouseholdIncomePerAdult();
    this.adultsAndIncome.adults.forEach(adult => adult.incomeBeforeTaxation = income);
  }

  private getHouseholdIncomePerAdult() {
    let income = this.store.welcomeAssessment.householdIncomeValue;
    income /= this.adultsAndIncome.adults.length;
    return income;
  }

  isFinishCountry() {
    return Country.FI == this.country;
  }

  onIncomeBeforeTaxSliderChange(adult) {
    this.setIncomeAfterTaxValueAutomatic(adult);
  }

  private setIncomeAfterTaxValueAutomatic(adult) {
    this.taxationService.getIncomeAfterTaxation(adult.incomeBeforeTaxation, this.country)
      .subscribe(value => adult.incomeAfterTaxation = value.amount);
  }

  goNext() {
    if (this.adultsAndIncome.manualIncomeAfterTax) {
      this.setDefaultIncomeBeforeTaxation();
    }
    this.financialOverviewService.financialOverview.householdSituation.adults = this.adultsAndIncome.adults;
    this.router.navigate(['children']);
  }

  goToStart() {
    this.router.navigate(['']);
  }
}
