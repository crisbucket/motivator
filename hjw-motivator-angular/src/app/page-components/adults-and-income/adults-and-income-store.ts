import {CleaneablePageStore} from "../cleanable-page-store";
import {Adult} from "../../models/financial-overview/householdsituation/adult";

export class AdultsAndIncomeStore implements CleaneablePageStore {
  adultCount: string = '1';
  adults: Adult[] = [];
  manualIncomeAfterTax: boolean = false;

  tempIncomesBeforeTaxation: number[] = [];
  tempIncomesAfterTaxation: number[] = [];

  clean() {
    this.adultCount = '1';
    this.adults = [];
    this.manualIncomeAfterTax = false;
    this.tempIncomesAfterTaxation = [];
    this.tempIncomesBeforeTaxation = [];
  }

  storeTempIncomesBeforeTaxation() {
    this.tempIncomesBeforeTaxation = this.adults.map(a => a.incomeBeforeTaxation);
  }

  restoreIncomesBeforeTaxationFromTemp() {
    for (var i = 0; i < this.tempIncomesBeforeTaxation.length; i++) {
      this.adults[i].incomeBeforeTaxation = this.tempIncomesBeforeTaxation[i];
    }
  }

  storeTempIncomesAfterTaxation() {
    this.tempIncomesAfterTaxation = this.adults.map(a => a.incomeAfterTaxation);
  }

  private restoreIncomeAfterTaxation() {
    for (var i = 0; i < this.tempIncomesAfterTaxation.length; i++) {
      this.adults[i].incomeAfterTaxation = this.tempIncomesAfterTaxation[i];
    }
  }

  totalIncomeAfterTaxation(): number {
    return this.adults
      .map(adult => adult.incomeAfterTaxation)
      .reduce((a, b) => a + b, 0);
  }

  collateIncomeAfterTaxOnFirstAdult() {
    this.storeTempIncomesAfterTaxation();
    this.adults[0].incomeAfterTaxation = this.totalIncomeAfterTaxation();
  }

  distributeAmountAfterTax() {
    let income = this.adults[0].incomeAfterTaxation;
    if (this.tempIncomesAfterTaxation && this.tempIncomesAfterTaxation.reduce((a, b) => a + b, 0) == income) {
      this.restoreIncomeAfterTaxation();
    } else {
      this.distributeIncomeAfterTaxation(income);
    }
  }

  private distributeIncomeAfterTaxation(income: number) {
    income /= this.adults.length;
    this.adults.forEach(adult => {
      adult.incomeAfterTaxation = income;
    });
  }
}
