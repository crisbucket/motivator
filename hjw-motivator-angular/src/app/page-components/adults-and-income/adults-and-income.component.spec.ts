import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AdultsAndIncomeComponent} from './adults-and-income.component';
import {FormsModule} from "@angular/forms";
import {MaterialModule} from "../../material.module";
import {FinancialOverviewService} from "../../services/financial-overview.service";
import {TaxationService} from "../../services/taxation.service";
import {RouterTestingModule} from "@angular/router/testing";
import {PipesMockModule} from "../../pipes/pipes.mock.module";
import {HjValueSliderComponent} from "../../common-components/hj-value-slider/hj-value-slider.component";
import {ProgressDotsComponent} from "../progress/progress-dots.component";
import {PageStoreService} from "../../services/page-store.service";
import {HjSwitchComponent} from "../../common-components/hj-switch/hj-switch.component";
import {FormatPipe} from "../../pipes/format.pipe";
import {TooltipModule} from "../../common-components/tooltip/tooltip.module";
import {FinancialOverview} from "../../models/financial-overview/financial-overview";
import {asyncData} from "../../testing/async-observable-helpers";
import {Adult} from "../../models/financial-overview/householdsituation/adult";

describe('AdultsAndIncomeComponent', () => {
  let component: AdultsAndIncomeComponent;
  let fixture: ComponentFixture<AdultsAndIncomeComponent>;
  const financialOverviewSpy = new FinancialOverviewService(null, null, null, null);
  const taxationServiceSpy = jasmine.createSpyObj('TaxationService', ['getIncomeAfterTaxation']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdultsAndIncomeComponent, HjValueSliderComponent, ProgressDotsComponent, HjSwitchComponent, FormatPipe ],
      providers: [
        { provide: FinancialOverviewService, useValue: financialOverviewSpy },
        { provide: TaxationService, useValue: taxationServiceSpy },
        { provide: PageStoreService, useClass: PageStoreService},
      ],
      imports: [MaterialModule, FormsModule, PipesMockModule, TooltipModule, RouterTestingModule.withRoutes([])]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    spyOnProperty(financialOverviewSpy, 'financialOverview', 'get').and.returnValue(new FinancialOverview());
    taxationServiceSpy.getIncomeAfterTaxation.and.returnValue(asyncData({amount: 1}));
    fixture = TestBed.createComponent(AdultsAndIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('User changes the number of people involved in the purchase (one => two)', () => {
    component.store.welcomeAssessment.householdIncomeValue = 25000;
    selectTwoAdults();
    expect(component.adultsAndIncome.adults.length).toEqual(2);
    expect(component.adultsAndIncome.adults[0].incomeBeforeTaxation).toEqual(12500);
    expect(component.adultsAndIncome.adults[1].incomeBeforeTaxation).toEqual(12500);
  });

  it('User changes the number of people involved in the purchase (two => one)', () => {
    component.store.welcomeAssessment.householdIncomeValue = 25000;
    component.adultsAndIncome.adults.push(new Adult());
    selectOneAdult();
    expect(component.adultsAndIncome.adults.length).toEqual(1);
    expect(component.adultsAndIncome.adults[0].incomeBeforeTaxation).toEqual(25000);
  });

  it('User changes to "input income after tax" for single adult', () => {
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    component.onIncomeAfterTaxSwitchChanged();
    expect(component.adultsAndIncome.adults.length).toEqual(1);
    expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(20000);
  });

  it('User changes to "input income after tax" for two adults', () => {
    component.adultsAndIncome.adults.push(new Adult());
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    component.store.adultsAndIncome.adults[1].incomeAfterTaxation = 30000;
    component.onIncomeAfterTaxSwitchChanged();
    expect(component.adultsAndIncome.adults.length).toEqual(2);
    expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(20000);
    expect(component.adultsAndIncome.adults[1].incomeAfterTaxation).toEqual(30000);
  });

  it('User changes back to "input income before tax" for single adult', async() => {
    component.store.adultsAndIncome.adults[0].incomeBeforeTaxation = 30000;
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    setManualIncomeAfterTax();
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 10000;
    setIncomeBeforeTax();
    expect(component.adultsAndIncome.adults.length).toEqual(1);
    expect(component.adultsAndIncome.adults[0].incomeBeforeTaxation).toEqual(30000);
    fixture.whenStable().then(() => {
      expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(1);
    })
  });

  it('User changes to "input income after tax", and then changes the number of adults (one => two)', () => {
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    setManualIncomeAfterTax();
    selectTwoAdults();
    expect(component.adultsAndIncome.adults.length).toEqual(2);
    expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(10000);
    expect(component.adultsAndIncome.adults[1].incomeAfterTaxation).toEqual(10000);
  });

  it('User changes to "input income after tax", and then changes the number of adults (two => one)', () => {
    component.adultsAndIncome.adults.push(new Adult());
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    component.store.adultsAndIncome.adults[1].incomeAfterTaxation = 15000;
    setManualIncomeAfterTax();
    selectOneAdult();
    expect(component.adultsAndIncome.adults.length).toEqual(1);
    expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(35000);
  });

  it('User makes changes to income after tax and then changes the number of adults (two => one)', () => {
    component.adultsAndIncome.adults.push(new Adult());
    component.adultsAndIncome.manualIncomeAfterTax = true;
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    component.store.adultsAndIncome.adults[1].incomeAfterTaxation = 15000;
    selectOneAdult();
    expect(component.adultsAndIncome.adults.length).toEqual(1);
    expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(35000);
  });

  it('User makes changes to income after tax and then changes the number of adults (two => one) and returns to the previous number of adults (one => two) then original values stay if the total income is not changed', () => {
    component.adultsAndIncome.adults.push(new Adult());
    component.adultsAndIncome.manualIncomeAfterTax = true;
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    component.store.adultsAndIncome.adults[1].incomeAfterTaxation = 15000;
    selectOneAdult();
    selectTwoAdults();
    expect(component.adultsAndIncome.adults.length).toEqual(2);
    expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(20000);
    expect(component.adultsAndIncome.adults[1].incomeAfterTaxation).toEqual(15000);
  });

  it('User makes changes to income after tax and then changes the number of adults (two => one) and returns to the previous number of adults (one => two) then if total income differs than after tax is distributed 50/50', () => {
    component.adultsAndIncome.adults.push(new Adult());
    component.adultsAndIncome.manualIncomeAfterTax = true;
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 20000;
    component.store.adultsAndIncome.adults[1].incomeAfterTaxation = 15000;
    selectOneAdult();
    component.store.adultsAndIncome.adults[0].incomeAfterTaxation = 32000;
    selectTwoAdults();
    expect(component.adultsAndIncome.adults.length).toEqual(2);
    expect(component.adultsAndIncome.adults[0].incomeAfterTaxation).toEqual(16000);
    expect(component.adultsAndIncome.adults[1].incomeAfterTaxation).toEqual(16000);
  });

  function selectTwoAdults() {
    component.adultsAndIncome.adultCount = '2';
    component.onAdultCountChanged(null);
  }

  function selectOneAdult() {
    component.adultsAndIncome.adultCount = '1';
    component.onAdultCountChanged(null);
  }

  function setManualIncomeAfterTax() {
    component.store.adultsAndIncome.manualIncomeAfterTax = true;
    component.onIncomeAfterTaxSwitchChanged();
  }

  function setIncomeBeforeTax() {
    component.store.adultsAndIncome.manualIncomeAfterTax = false;
    component.onIncomeAfterTaxSwitchChanged();
  }
});
