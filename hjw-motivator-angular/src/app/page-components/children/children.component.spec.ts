import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChildrenComponent} from './children.component';
import {CommonComponentsModule} from "../../common-components/common-components.module";
import {MaterialModule} from "../../material.module";
import {FormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";
import {FinancialOverviewService} from "../../services/financial-overview.service";
import {PipesMockModule} from "../../pipes/pipes.mock.module";
import {PageStoreService} from "../../services/page-store.service";
import {ChildrenSupportService} from "../../services/children-support.service";
import {FinancialOverview} from "../../models/financial-overview/financial-overview";

describe('ChildrenComponent', () => {
  let component: ChildrenComponent;
  const financialOverviewSpy = new FinancialOverviewService(null, null, null, null);
  const childrenSupportServiceSpy = jasmine.createSpyObj('ChildrenSupportService', ['childrenSupportService']);

  let fixture: ComponentFixture<ChildrenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChildrenComponent],
      providers: [
        {provide: FinancialOverviewService, useValue: financialOverviewSpy},
        {provide: ChildrenSupportService, useValue: childrenSupportServiceSpy},
        {provide: PageStoreService, useValue: new PageStoreService()}
      ],
      imports: [MaterialModule, FormsModule, CommonComponentsModule, PipesMockModule,
        RouterTestingModule.withRoutes([])]
    }).compileComponents();
  }));

  beforeEach(() => {
    spyOnProperty(financialOverviewSpy, 'financialOverview', 'get').and.returnValue(new FinancialOverview());
    fixture = TestBed.createComponent(ChildrenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
