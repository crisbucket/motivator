import {Child} from "../../models/financial-overview/householdsituation/child";
import {CleaneablePageStore} from "../cleanable-page-store";

export class ChildrenStore implements CleaneablePageStore{
  childCount = '0';
  children: Child[] = [];
  childrenSupportIncome = 0;

  clean() {
    this.childCount = '0';
    this.children = [];
    this.childrenSupportIncome = 0;
  }
}
