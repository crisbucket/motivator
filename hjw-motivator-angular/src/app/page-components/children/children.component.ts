import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FinancialOverviewService} from '../../services/financial-overview.service';
import {Child} from '../../models/financial-overview/householdsituation/child';
import {PageStoreService} from "../../services/page-store.service";
import {ChildrenSupportService} from "../../services/children-support.service";
import {Subscription} from "../../../../node_modules/rxjs/Rx";
import {CountryCurrency} from "../../models/country-currency";
import {Currency} from "../../models/currency";

@Component({
  selector: 'hjwm-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss']
})
export class ChildrenComponent implements OnInit {
  pendingChildrenSupportRequest: Subscription;
  currency: Currency;

  constructor(private router: Router,
              private financialOverviewService: FinancialOverviewService,
              private childrenSupportService: ChildrenSupportService,
              public store: PageStoreService) {
  }

  ngOnInit() {
    this.currency = CountryCurrency.getByCountry(this.financialOverviewService.financialOverview.householdSituation.country);
  }

  onChildrenCountChange() {
    const childCount: number = Number(this.store.children.childCount);
    if (this.store.children.children.length > childCount) {
      this.store.children.children.pop();
    } else {
      for (let count = this.store.children.children.length; count < childCount; count++) {
        this.addAChild();
      }
    }
    this.getChildrenSupportIncome();
  }

  private getChildrenSupportIncome() {
    let householdSituation = this.financialOverviewService.financialOverview.householdSituation;
    householdSituation.children = this.store.children.children;
    if (this.pendingChildrenSupportRequest) {
      this.pendingChildrenSupportRequest.unsubscribe();
    }

    this.pendingChildrenSupportRequest = this.childrenSupportService.getChildrenSupportIncome(householdSituation).subscribe(
      value =>
        this.store.children.childrenSupportIncome = value.amount);
  }

  addAChild() {
    const child = new Child();
    child.age = 0;
    this.store.children.children.unshift(child);
  }

  onSliderChange() {
    this.getChildrenSupportIncome();
  }

  goNext() {
    this.financialOverviewService.financialOverview.householdSituation.children = this.store.children.children;
    this.financialOverviewService.financialOverview.childrenSupportIncome = this.store.children.childrenSupportIncome;
    console.log(this.financialOverviewService.financialOverview);
    this.router.navigate(['cars']);
  }

  goBack() {
    this.router.navigate(['adults']);
  }

  goToStart() {
    this.router.navigate(['']);
  }
}
