import {Component,OnInit} from '@angular/core';
import {FinancialOverviewService} from './services/financial-overview.service';
import {NordicMotivatorApp} from './nordic-motivator-app';
import {AppConstants} from './appconstants';

@Component({
  selector: '[data-application-name=hjnm]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title:string = '';

  constructor(private financialOverviewService: FinancialOverviewService, private hjnmTridionApp:NordicMotivatorApp, private appConstants : AppConstants) {
    this.title = this.appConstants.APP_ID;
  }
  ngOnInit() {
    this.hjnmTridionApp.setTridionAppId(this.appConstants.APP_ID);
  }
  startWizard () {
    //

  }

}
