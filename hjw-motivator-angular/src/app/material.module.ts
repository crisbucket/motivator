import {NgModule} from '@angular/core';
import {
  MatButtonModule,
  MatDialogModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatRadioModule,
  MatSelectModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatStepperModule,
} from '@angular/material';


@NgModule({
  imports: [
    MatFormFieldModule,
    MatSliderModule,
    MatInputModule,
    MatStepperModule,
    MatButtonModule,
    MatIconModule,
    MatSlideToggleModule,
    MatSelectModule,
    MatRadioModule,
    MatDialogModule,
  ],
  exports: [
    MatFormFieldModule,
    MatSliderModule,
    MatIconModule,
    MatInputModule,
    MatSlideToggleModule,
    MatStepperModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatDialogModule
  ]
})

export class MaterialModule {}
