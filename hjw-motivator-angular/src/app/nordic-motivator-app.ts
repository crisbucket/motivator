import {Injectable} from '@angular/core';
import {ConfigService} from './services/config.service';
import {DWAWebapp} from 'dwa-tridion-webapp';

@Injectable()
export class NordicMotivatorApp {
  tridionApp: any;
  constructor(private configService: ConfigService) {
    this.tridionApp = new DWAWebapp(configService.getTriditionAppConfig());
  }
  setTridionAppId(appId: string) {
    this.tridionApp.setId(appId);
  }
  getLabels() {
    return this.tridionApp.labels;
  }
 // add functionality here to extend Tridion App or which is specific to Tridion
}
