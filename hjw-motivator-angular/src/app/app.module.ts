import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MAT_LABEL_GLOBAL_OPTIONS} from '@angular/material';
import {APP_BASE_HREF, registerLocaleData} from '@angular/common';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {FinancialOverviewService} from './services/financial-overview.service';
import {WindowRef} from './services/window.service';
import {MaterialModule} from './material.module';
import {ChartsModule} from 'ng2-charts';
import {HttpClientModule} from '@angular/common/http';
import {ModelBudgetService} from './services/model-budget.service';
import localeDe from '@angular/common/locales/de';
import localeSe from '@angular/common/locales/se';
import {ConfigService} from './services/config.service';
import {AppConstants} from './appconstants';
import {environment} from '../environments/environment';
import {ServiceMocks} from './app.mock.providers';
import {NordicMotivatorApp} from './nordic-motivator-app';
import {CreditAssessmentService} from './services/credit-assessment.service';
import {CommonComponentsModule} from './common-components/common-components.module';
import {PageComponentsModule} from './page-components/page-components.module';
import {TaxationService} from './services/taxation.service';
import {ChildrenSupportService} from './services/children-support.service';
import {MortgageComputationService} from './services/mortgage-computation.service';
import {PipesModule} from "./pipes/pipes.module";
import {PageStoreService} from "./services/page-store.service";
import {DashboardElementDescriptionService} from "./services/dashboard-element-description.service";
import {DashboardService} from "./page-components/dashboard/dashboard-service";
import {ReplaceLabelPipe} from "./pipes/replace-label.pipe";

const defaultProviders: any = [
  FinancialOverviewService,
  CreditAssessmentService,
  TaxationService,
  ChildrenSupportService,
  MortgageComputationService,
  ModelBudgetService,
  PageStoreService,
  ConfigService,
  DashboardService,
  DashboardElementDescriptionService,
  AppConstants,
  NordicMotivatorApp,
  ReplaceLabelPipe,
  WindowRef,
  {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}},
  {provide: APP_BASE_HREF, useValue: '/'}
];
const providers: any = environment['mock'] ? [...ServiceMocks, ...defaultProviders] : defaultProviders;

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChartsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    CommonComponentsModule,
    PageComponentsModule,
    PipesModule
  ],
  exports: [
    RouterModule,
  ],
  providers: [providers],
  bootstrap: [AppComponent]
})
export class AppModule {
}

registerLocaleData(localeDe);
registerLocaleData(localeSe);
