import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConstants} from '../appconstants';
import {Country} from "../models/country";
import {DebtMonthlyPayment} from "../models/dashboard/debt-monthly-payment";
import {Observable} from "rxjs/Observable";

@Injectable()
export class MortgageComputationService {
  constructor(private http: HttpClient, private appConstants: AppConstants) {
  }

  public getMonthlyPaymentForLoanAmountByMarketRate(loanAmount: number, country: Country) {
    return this.http.get<number>(
      this.appConstants.MORTGAGE_COMPUTATION_URL + "/" + loanAmount + "/monthlyPayment/marketRate",
      {headers: new HttpHeaders().append('country', country.toString())}
    );
  }

  public getMonthlyPaymentForNonCollateralDebt(debtAmount: number, country: Country) {
    return this.http.get<DebtMonthlyPayment>(
      this.appConstants.MORTGAGE_COMPUTATION_URL + "/debt/nonCollateral/" + debtAmount + "/monthlyPayment",
      {headers: new HttpHeaders().append('country', country.toString())}
    );
  }

  public getMonthlyPaymentForOtherMortgageDebt(debtAmount: number, country: Country) {
    return this.http.get<DebtMonthlyPayment>(
      this.appConstants.MORTGAGE_COMPUTATION_URL + "/debt/otherMortgage/" + debtAmount + "/monthlyPayment",
      {headers: new HttpHeaders().append('country', country.toString())}
    );
  }
}
