import {Injectable} from "@angular/core";
import {PropertyStore} from "../page-components/property/property-store";
import {CarsStore} from "../page-components/cars/cars-store";
import {ChildrenStore} from "../page-components/children/children-store";
import {AdultsAndIncomeStore} from "../page-components/adults-and-income/adults-and-income-store";
import {WelcomeAssessmentStore} from "../page-components/welcome-assessment/welcome-assessment-store.service";

@Injectable()
export class PageStoreService {
  property: PropertyStore = new PropertyStore();
  cars: CarsStore = new CarsStore();
  children: ChildrenStore = new ChildrenStore();
  adultsAndIncome: AdultsAndIncomeStore = new AdultsAndIncomeStore();
  welcomeAssessment: WelcomeAssessmentStore = new WelcomeAssessmentStore();

  public cleanStores() {
    this.property.clean();
    this.cars.clean();
    this.children.clean();
    this.adultsAndIncome.clean();
    this.welcomeAssessment.clean();
  }
}
