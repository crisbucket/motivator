import {ModelBudgetService} from './model-budget.service';
import {ModelBudget} from '../models/model-budget/model-budget';

import {FinancialOverviewService} from './financial-overview.service';
import {FinancialOverview} from "../models/financial-overview/financial-overview";

describe('FinancialOverviewService', () => {
  let httpClientSpy: { put: jasmine.Spy, get: jasmine.Spy };
  let modelBudgetSpy: { getModelBudget: jasmine.Spy };
  let appConstantsSpy: { MODEL_BUDGET_SERVICE_URL: jasmine.Spy };
  let mortgageComputationSpy: { getMonthlyPaymentForOtherMortgageDebt: jasmine.Spy};
  let service: FinancialOverviewService;

  const financialOverview = new FinancialOverview();
  const modelBudget = new ModelBudget([]);

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['put', 'get']);
    modelBudgetSpy = jasmine.createSpyObj('ModelBudgetService', ['getModelBudget']);
    appConstantsSpy = jasmine.createSpyObj('AppConstants', ['FINANCIAL_OVERVIEW_SERVICE_URL']);
    mortgageComputationSpy = jasmine.createSpyObj('MortgageComputationService', ['getMonthlyPaymentForOtherMortgageDebt']);
    service = new FinancialOverviewService(<any> httpClientSpy, <any>modelBudgetSpy, <any> mortgageComputationSpy, <any> appConstantsSpy);
    service.setFinancialOverviewId('financial-overview-id');
  });
});
