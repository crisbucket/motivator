import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AppConstants} from "../appconstants";
import {Observable} from "rxjs/index";
import {HouseholdSituation} from "../models/financial-overview/householdsituation/household-situation";
import {IncomeAfterTaxation} from "../models/taxation/income-after-taxation";

@Injectable()
export class ChildrenSupportService {

  constructor(private http: HttpClient,
              private appConstants: AppConstants) {
  }

  public getChildrenSupportIncome(householdSituation: HouseholdSituation): Observable<IncomeAfterTaxation> {
    return this.http.post<IncomeAfterTaxation>(this.appConstants.CHILDREN_SUPPORT_URL, householdSituation);
  }
}
