import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreditAssessment} from '../models/credit-assessment/credit-assessment';
import {AppConstants} from '../appconstants';
import {CreditAssessmentRequest} from '../models/credit-assessment/credit-assessment-request';
import {Observable} from 'rxjs/Rx';
import {AffordabilityThresholds} from "../models/credit-assessment/affordability-thresholds";

@Injectable()
export class CreditAssessmentService {
  constructor(private http: HttpClient, private appConstants: AppConstants) {
  }

  public getCreditAssessmentOnDTI(creditAssessmentRequest: CreditAssessmentRequest): Observable<CreditAssessment> {
    return this.http.post<CreditAssessment>(`${this.appConstants.CREDIT_ASSESSMENT_SERVICE_URL}`, creditAssessmentRequest);
  }

  public getAffordabilityThresholds(financialOverviewId: string){
    return this.http.get<AffordabilityThresholds>(`${this.appConstants.CREDIT_ASSESSMENT_SERVICE_URL}${financialOverviewId}/affordabilityThresholds`);
  }
}
