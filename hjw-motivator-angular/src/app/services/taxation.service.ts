import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {IncomeAfterTaxation} from "../models/taxation/income-after-taxation";
import {AppConstants} from "../appconstants";
import {Observable} from "rxjs/index";
import {Country} from "../models/country";

@Injectable()
export class TaxationService {

  constructor(private http: HttpClient,
              private appConstants: AppConstants){
  }

  public getIncomeAfterTaxation(income: number, country: Country): Observable<IncomeAfterTaxation> {
    return this.http.get<IncomeAfterTaxation>(
      this.appConstants.TAXATION_SERVICE_URL + "/" + country +"?incomeBeforeTaxation="+income);
  }

  private asIncomeAfterTaxation(value) {
    return Object.assign(new IncomeAfterTaxation(), value as IncomeAfterTaxation);
  }
}
