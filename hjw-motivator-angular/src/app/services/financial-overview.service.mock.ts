import {Injectable} from '@angular/core';
import FinancialOverviewServiceMock from './mocks/financial-overview.service.mock.json';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {FinancialOverview} from "../models/financial-overview/financial-overview";

@Injectable()
export class FinancialOverviewServiceMockInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.endsWith('financialOverview/') && request.method === 'POST') {
      return of(new HttpResponse<FinancialOverview>({body: FinancialOverviewServiceMock.financialoverview, status: 200}));
    }
    if (request.url.endsWith('childrenSupportIncome') && request.method === 'PUT') {
      return of(new HttpResponse<FinancialOverview>({body: FinancialOverviewServiceMock.financialoverview, status: 200}));
    }
    if (request.url.endsWith('asset/downpayment') && request.method === 'PUT') {
      return of(new HttpResponse<FinancialOverview>({body: FinancialOverviewServiceMock.financialoverview, status: 200}));
    }
    if (request.url.endsWith('debt') && request.method === 'PUT') {
      return of(new HttpResponse<FinancialOverview>({body: FinancialOverviewServiceMock.financialoverview, status: 200}));
    }
    if (request.url.endsWith('family') && request.method === 'PUT') {
      return of(new HttpResponse<FinancialOverview>({body: FinancialOverviewServiceMock.financialoverview, status: 200}));
    }
    if (request.url.endsWith('cars') && request.method === 'PUT') {
      return of(new HttpResponse<FinancialOverview>({body: FinancialOverviewServiceMock.financialoverview, status: 200}));
    }
    if (request.url.indexOf('financialOverview') !== -1 && request.method === 'GET') {
      return of(new HttpResponse<FinancialOverview>({body: FinancialOverviewServiceMock.financialoverview, status: 200}));
    }
    return next.handle(request);
  }
}
