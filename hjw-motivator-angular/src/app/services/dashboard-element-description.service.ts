import {Injectable} from "@angular/core";
import {ExpenseGroup} from "../models/model-budget/expense-group";
import {FinancialOverview} from "../models/financial-overview/financial-overview";
import {ReplaceLabelPipe} from "../pipes/replace-label.pipe";
import {FamilySituation} from "../models/financial-overview/householdsituation/family-situation";

@Injectable()
export class DashboardElementDescriptionService {
  public getExpenseGroupDescription(expenseGroup: ExpenseGroup, financialOverview: FinancialOverview, replaceLabelPipe :ReplaceLabelPipe) {
    let description;
    switch (expenseGroup.groupType) {
      case 'EVERYDAY_EXPENSES':
        description = replaceLabelPipe.transform('EVERYDAY_EXPENSES_DESC');
        break;
      case 'HOUSING_EXPENSES':
        description = replaceLabelPipe.transform('HOUSING_EXPENSES_DESC');
        break;
      case 'TRANSPORTATION_EXPENSES':
        description = this.transportationExpenses(financialOverview, replaceLabelPipe);
      break;
    }

    return description;
  }

  public getFamilySituationDescription(familySituation: FamilySituation, replaceLabelPipe :ReplaceLabelPipe) :string {
    return "";
  }

  private transportationExpenses(financialOverview: FinancialOverview, replaceLabelPipe :ReplaceLabelPipe) {
    let resultLabel = "";
    let cars = financialOverview.householdSituation.numberOfCars;
    if(cars == 1) {
      resultLabel =  cars + " "  + replaceLabelPipe.transform("TRANSPORTATION_EXPENSES_DESC_CAR");
    } else {
      resultLabel = cars + " " +replaceLabelPipe.transform("TRANSPORTATION_EXPENSES_DESC_CARS");
    }
    if(financialOverview.householdSituation.otherTransportationCost > 0) {
      resultLabel += ", " + replaceLabelPipe.transform("TRANSPORTATION_EXPENSES_DESC_ADD_COST");
    }

    return resultLabel;
  }
}
