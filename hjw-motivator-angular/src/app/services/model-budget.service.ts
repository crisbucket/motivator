import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ModelBudget} from '../models/model-budget/model-budget';
import {HouseholdSituation} from '../models/financial-overview/householdsituation/household-situation';
import {Observable} from 'rxjs';
import {AppConstants} from '../appconstants';

@Injectable()
export class ModelBudgetService {

  constructor(private http: HttpClient, private appConstants: AppConstants) {
  }

  public getModelBudget(householdSituation: HouseholdSituation): Observable<ModelBudget> {
    return this.http.post<ModelBudget>(this.appConstants.MODEL_BUDGET_SERVICE_URL, householdSituation);
  }
}
