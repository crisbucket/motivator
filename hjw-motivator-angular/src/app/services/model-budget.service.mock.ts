import { Injectable } from '@angular/core';
import ModelBudgetMock from './mocks/model-budget.service.mock.json';
import {
  HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable,of } from 'rxjs';
import { ModelBudget } from '../models/model-budget/model-budget';

@Injectable()
export class ModelBudgetServiceMockInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(request.url.endsWith('model-budget/') && request.method === 'POST'){
      return of(new HttpResponse<ModelBudget>({body:ModelBudgetMock,status:200}));
    }

    return next.handle(request);
  }
}
