import {Injectable} from '@angular/core';
import localTridionConfig from '../tridion.config.json';
import {AppConstants} from '../appconstants';
import {WindowRef} from "./window.service";

@Injectable()
export class ConfigService {

  private tridionConfig : any = {};
  constructor(private appConstants : AppConstants, private window: WindowRef) {
     let config = window.nativeWindow.hjnm ? window.nativeWindow.hjnm : localTridionConfig;
     let appIdOption = config.metadata.applicationOptions.filter(appOption => appOption.id[0] === 'API_HOST')[0];
     if(appIdOption && appConstants.API_HOST !== ""){
       appIdOption.applicationOption.push(appConstants.API_HOST);
     }
     this.tridionConfig = config;
  }

  public getTriditionAppConfig() {
      return this.tridionConfig;
  }

}
