import {Injectable} from '@angular/core';
import CreditAssessmentMock from './mocks/credit-assessment.service.mock.json';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {CreditAssessment} from '../models/credit-assessment/credit-assessment';

@Injectable()
export class CreditAssessmentServiceMockInterceptor implements HttpInterceptor {
    constructor() {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      if (request.url.indexOf('credit-assessment') !== -1 && !request.params.has('affordabilityAmount')
        && request.method === 'GET') {
        return of(new HttpResponse<CreditAssessment>({body: CreditAssessmentMock, status: 200}));
      }
      if (request.params.has('affordabilityAmount') && request.method === 'GET') {
        return of(new HttpResponse<CreditAssessment>({body: CreditAssessmentMock, status: 200}));
      }
      return next.handle(request);
    }
}
