import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ModelBudgetService} from './model-budget.service';
import {Observable, of} from 'rxjs';
import {map, mergeMap, tap} from 'rxjs/operators';
import {ExpenseGroup} from '../models/model-budget/expense-group';
import {AppConstants} from '../appconstants';
import {catchError} from 'rxjs/internal/operators';
import {FinancialOverview} from '../models/financial-overview/financial-overview';
import {MortgageComputationService} from "./mortgage-computation.service";
import {DebtType} from "../models/financial-overview/debt-type";
import {ReplaySubject} from "rxjs/ReplaySubject";

@Injectable()
export class FinancialOverviewService {
  private _financialOverview: FinancialOverview;
  private _financialOverview$: ReplaySubject<FinancialOverview> = new ReplaySubject<FinancialOverview>(1);

  get financialOverview$() {
    return this._financialOverview$.asObservable();
  }

  constructor(private http: HttpClient,
              private modelBudgetService: ModelBudgetService,
              private mortgageComputationService: MortgageComputationService,
              private appConstants: AppConstants) {
    this._financialOverview = new FinancialOverview();
    this.createFinancialOverview = this.createFinancialOverview.bind(this);

  }

  public get financialOverview(): FinancialOverview {
    return this._financialOverview;
  }

  public createOrLoadSavedFinancialOverview(): Observable<FinancialOverview> {
    if (this.getFinancialOverviewId()) {
      return this.getFinancialOverview();
    }
    return this.modelBudgetService.getModelBudget(this.financialOverview.householdSituation).pipe(
      mergeMap(modelBudget => {
        this.financialOverview.expenseGroups = modelBudget.expenseGroups;

        return this.mortgageComputationService
          .getMonthlyPaymentForNonCollateralDebt(this.financialOverview.findDebtByType(DebtType.NCCL).amount, this.financialOverview.householdSituation.country)
          .pipe(mergeMap(debtMonthlyPayment => {
            this.financialOverview.updateMonthlyPaymentForDebt(DebtType.NCCL, debtMonthlyPayment);
            return this.mortgageComputationService
              .getMonthlyPaymentForOtherMortgageDebt(this.financialOverview.findDebtByType(DebtType.OTHER_MORTGAGE).amount, this.financialOverview.householdSituation.country)
              .pipe(mergeMap(debtMonthlyPayment => {
                this.financialOverview.updateMonthlyPaymentForDebt(DebtType.OTHER_MORTGAGE, debtMonthlyPayment);
                return this.createFinancialOverview();
              }));
          }));
      }),
      tap(value => this._financialOverview = this.asFinancialOverview(value)));
  }

  private createFinancialOverview() {
    return this.http.post<FinancialOverview>(this.appConstants.FINANCIAL_OVERVIEW_SERVICE_URL, this.financialOverview.toAPI()).pipe(
      map(value => {
        const financialOverview = this.asFinancialOverview(value);
        this.setFinancialOverviewId(financialOverview.id);
        return financialOverview;
      }),
      catchError(error => {
        console.log(error.message);
        return of(error);
      }),
      tap(value => this._financialOverview = this.asFinancialOverview(value))
    );
  }

  public updateFinancialOverview() {
    console.log(this.financialOverview);
    console.log(this.financialOverview.toAPI());
    return this.http.put<FinancialOverview>(this.appConstants.FINANCIAL_OVERVIEW_SERVICE_URL + this.getFinancialOverviewId(), this.financialOverview.toAPI()).pipe(
      map(value => {
        const financialOverview = this.asFinancialOverview(value);
        this._financialOverview = this.asFinancialOverview(value)
        console.log(financialOverview);
        this.setFinancialOverviewId(financialOverview.id);
        return financialOverview;
      }),
      catchError(error => {
        console.log(error.message);
        return of(error);
      })
    );
  }

  public getFinancialOverview(): Observable<FinancialOverview> {
    return this.http.get<FinancialOverview>(this.appConstants.FINANCIAL_OVERVIEW_SERVICE_URL + this.getFinancialOverviewId()).pipe(
      map(value => this.asFinancialOverview(value)),
      tap(value => this._financialOverview = this.asFinancialOverview(value))
    );
  }

  private asFinancialOverview(value) {
    const financialOverview = Object.assign(new FinancialOverview(), value as FinancialOverview);
    if (financialOverview) {
      const expenseGroups = financialOverview.expenseGroups;
      expenseGroups.map(g => Object.assign(new ExpenseGroup(), g as ExpenseGroup));
    }
    return financialOverview;
  }

  public fillExpenses(expenseGroups: ExpenseGroup[]) {
    this.http.put(this.appConstants.FINANCIAL_OVERVIEW_SERVICE_URL + this.getFinancialOverviewId() + '/expenses', {expenseGroups: expenseGroups}).pipe(
      map(value => {
        const financialOverview = this.asFinancialOverview(value);
        this._financialOverview = this.asFinancialOverview(value)
        this.setFinancialOverviewId(financialOverview.id);
        return financialOverview;
      }));
  }

  getFinancialOverviewId(): string {
    return sessionStorage.getItem('financialOverviewId');
  }

  setFinancialOverviewId(financialOverviewId: string) {
    sessionStorage.setItem('financialOverviewId', financialOverviewId);
  }

  clearFinancialOverviewId() {
    sessionStorage.removeItem('financialOverviewId');
  }
}
