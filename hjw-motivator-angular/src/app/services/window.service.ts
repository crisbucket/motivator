import { Injectable } from '@angular/core';

//Apparently $window was only a AngularJS thing

function _window() : any {
  return window;
}

@Injectable()
export class WindowRef {
  get nativeWindow() : any {
    return _window();
  }
}
