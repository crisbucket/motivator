import {HttpErrorResponse} from '@angular/common/http';
import {HouseholdSituation} from '../models/financial-overview/householdsituation/household-situation';
import {ModelBudget} from '../models/model-budget/model-budget';
import {asyncData, asyncError} from '../testing/async-observable-helpers';
import {ModelBudgetService} from './model-budget.service';

describe('ModelBudgetService', () => {
  let httpClientSpy: { post: jasmine.Spy };
  let appConstantsSpy: { MODEL_BUDGET_SERVICE_URL: jasmine.Spy };
  let service: ModelBudgetService;
  const sampleHouseholdSituation = new HouseholdSituation();

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    appConstantsSpy = jasmine.createSpyObj('AppConstants', ['MODEL_BUDGET_SERVICE_URL']);
    service = new ModelBudgetService(<any> httpClientSpy, <any> appConstantsSpy);
  });

  it('getModelBudget should return ModelBudget', () => {
    const expectedModelBudget: ModelBudget = new ModelBudget([]);
    httpClientSpy.post.and.returnValue(asyncData(expectedModelBudget));
    appConstantsSpy.MODEL_BUDGET_SERVICE_URL.and.returnValue('localhost');
    service.getModelBudget(sampleHouseholdSituation).subscribe(
      modelBudget => {
        expect(modelBudget).toEqual(expectedModelBudget, 'expected model budget');
      },
      fail
    );
  });

  it('getModelBudget should return an error when the server returns a 503', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'test 503 error',
      status: 503, statusText: 'Server error'
    });
    httpClientSpy.post.and.returnValue(asyncError(errorResponse));
    appConstantsSpy.MODEL_BUDGET_SERVICE_URL.and.returnValue('localhost');
    service.getModelBudget(sampleHouseholdSituation).subscribe(
      modelBudget => fail('expected an error, not ModelBudget'),
      error => expect(error.error).toContain('test 503 error')
    );
  });
});
