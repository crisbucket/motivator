import {EnvConfig} from './environment.config'
export const environment = {
  production: false,
  config: EnvConfig.LOCAL,
  API_HOST: "",
  API_HOSTS: {
    financialoverview: 'http://localhost:8311/',
    creditassessment: 'http://localhost:8313/',
    modelbudget: 'http://localhost:8999/',
    taxation: 'http://localhost:8490/',
    mortgagecomputation: 'http://localhost:8093/'
  },
  mock:true
};
