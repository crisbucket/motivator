import {EnvConfig} from './environment.config'
export const environment = {
  production: false,
  config: EnvConfig.LOCAL,
  API_HOST: "",
  API_HOSTS: {
    financialoverview: 'http://localhost:8450/',
    creditassessment: 'http://localhost:8470/',
    modelbudget: 'http://localhost:8460/',
    taxation: 'http://localhost:8490/',
    mortgagecomputation: 'http://localhost:8093/'
  },
  mock:false
};

import 'zone.js/dist/zone-error';
