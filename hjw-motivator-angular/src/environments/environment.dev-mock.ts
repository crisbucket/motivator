import {EnvConfig} from './environment.config'
export const environment = {
  production: false,
  config : EnvConfig.DEV,
  API_HOST:'http://dev.hjnm.com',
  API_HOSTS: {},
  mock:true
};
