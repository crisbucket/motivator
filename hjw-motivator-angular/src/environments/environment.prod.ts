import {EnvConfig} from './environment.config'

export const environment = {
  production: true,
  config : EnvConfig.PROD,
  API_HOST:'',
  API_HOSTS: {
    financialoverview: 'https://vda1cs0188:9060/hj/common/api/financialoverview/',
    creditassessment: 'https://vda1cs0188:9060/hj/common/api/creditassessment/',
    modelbudget: 'https://vda1cs0188:9060/hj/common/api/modelbudget/',
    taxation: 'https://vda1cs0188:9060/hj/common/api/taxation/',
    mortgagecomputation: 'https://ndfmortgageopencomputation-v1.mortgage.dev01.qaoneadr.local/'
  }
};
